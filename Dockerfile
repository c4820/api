# Stage 1: Compile and Build angular codebase
# Use official node image as the base image
FROM node:17-alpine3.15 as build

# Set the working directory
WORKDIR /app

# Add the source code to applications
COPY ./ /app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./app

# Install latest npm
RUN npm install -g npm@8.13.2

# Install all the dependencies
RUN npm install --legacy-peer-deps

# Generate the build of the applications
RUN node ./node_modules/@nestjs/cli/bin/nest build

CMD [ "node", "dist/main.js" ]

# Expose port 80
EXPOSE 3000
