import {Body, Controller, Delete, Get, HttpCode, HttpStatus, Inject, Param, Post, Put} from '@nestjs/common';
import {ApiOperation, ApiTags} from '@nestjs/swagger';
import {EXAMPLE_SERVICE} from '../../core-modules/database/constants/injectors';
import {IExampleService} from '../../core-modules/database/services/interfaces/example-service.interface';
import {Example} from '../../core-modules/database/models/example';
import {
    LenientAuthentication
} from '../../core-modules/services/authentication-service/decorators/authentication-metadata.decorator';
import {merge as lodashMerge} from 'lodash';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {AddExampleViewModel} from "../../../view-models/examples/add-example.view-model";
import {addExample, editExample} from "../../../constants/validator-builders/example-validation-builders";
import {EditExampleViewModel} from "../../../view-models/examples/edit-example.view-model";

@ApiTags('example')
@Controller('api/example')
export class ExampleController {

    //#region Constructor

    public constructor(@Inject(EXAMPLE_SERVICE)
                       protected readonly _exampleService: IExampleService,
                       @Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
    }

    //#endregion

    //#region Methods

    @Get(':id')
    @ApiOperation({
        description: 'Get an example by searching for its id.'
    })
    @HttpCode(HttpStatus.OK)
    @LenientAuthentication()
    public async getBydIdAsync(@Param('id') id: string): Promise<Example> {
        return this._exampleService.getByIdAsync(id);
    }

    @Get('by-component-id/:componentId')
    @ApiOperation({
        description: 'Get examples which belong to a specific component'
    })
    @HttpCode(HttpStatus.OK)
    @LenientAuthentication()
    public async getByComponentIdAsync(@Param('componentId') componentId: string): Promise<Example[]> {
        return this._exampleService.getByComponentIdAsync(componentId);
    }

    @Post()
    @ApiOperation({
        description: 'Create an example by using specific information'
    })
    @HttpCode(HttpStatus.OK)
    public async addAsync(@Body() command: AddExampleViewModel): Promise<Example> {
        await this._validationService.validateAsync(addExample, command, true);
        return this._exampleService.addAsync(command);
    }

    @Put('id')
    @ApiOperation({
        description: 'Edit an example by using its id.'
    })
    @HttpCode(HttpStatus.OK)
    public async editAsync(@Param(':id') id: string, command: EditExampleViewModel): Promise<Example> {
        const actualCommand = lodashMerge(new EditExampleViewModel(''), command);
        await this._validationService.validateAsync(editExample, command, true);

        return this._exampleService.editAsync(actualCommand);
    }


    @Delete(':id')
    @ApiOperation({
        description: 'Find and delete an example by using its id.'
    })
    @HttpCode(HttpStatus.OK)
    public async deleteByIdAsync(@Param('id') id: string): Promise<void> {
        return this._exampleService.deleteByIdAsync(id);
    }


    //#endregion

}
