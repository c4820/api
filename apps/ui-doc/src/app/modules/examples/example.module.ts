import {Inject, Module} from '@nestjs/common';
import {DatabaseModule} from '../../core-modules/database/database.module';
import {ExampleController} from './example.controller';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {
  addExampleValidatorBuilder,
  editExampleValidatorBuilder
} from "../../../constants/validator-builders/example-validation-builders";

@Module({
    imports: [
        DatabaseModule
    ],
    controllers: [
        ExampleController
    ]
})
export class ExampleModule {

    //#region Constructor

    public constructor(@Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
        this._validationService.with(addExampleValidatorBuilder);
        this._validationService.with(editExampleValidatorBuilder);
    }

    //#endregion

}
