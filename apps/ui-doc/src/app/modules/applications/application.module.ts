import {Inject, Module} from '@nestjs/common';
import {DatabaseModule} from '../../core-modules/database/database.module';
import {ApplicationController} from './application.controller';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {
  addApplicationValidatorBuilder,
  editApplicationValidatorBuilder
} from "../../../constants/validator-builders/application-validation-builders";

@Module({
    imports: [
        DatabaseModule
    ],
    controllers: [
        ApplicationController
    ],
    providers: []
})
export class ApplicationModule {

    //#region Constructor

    public constructor(@Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
        this._validationService.with(addApplicationValidatorBuilder);
        this._validationService.with(editApplicationValidatorBuilder);
    }

    //#endregion

}
