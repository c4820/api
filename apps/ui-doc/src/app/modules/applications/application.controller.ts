import {Body, Controller, Delete, Get, HttpCode, Inject, Param, Post, Put} from '@nestjs/common';
import {Application} from '../../core-modules/database/models/application';
import {APPLICATION_SERVICE, APPLICATION_VERSION_SERVICE} from '../../core-modules/database/constants/injectors';
import {IApplicationService} from '../../core-modules/database/services/interfaces/application-service.interface';
import {ApiOkResponse, ApiOperation, ApiTags} from '@nestjs/swagger';
import {ApplicationVersion} from '../../core-modules/database/models/application-version';
import {
    IApplicationVersionService
} from '../../core-modules/database/services/interfaces/application-version-service.interface';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {
    AllowAnonymous, LenientAuthentication
} from '../../core-modules/services/authentication-service/decorators/authentication-metadata.decorator';
import {addApplication, editApplication} from "../../../constants/validator-builders/application-validation-builders";
import {EditApplicationViewModel} from "../../../view-models/applications/edit-application.view-model";
import {AddApplicationViewModel} from "../../../view-models/applications/add-application.view-model";

@ApiTags('application')
@Controller('api/application')
export class ApplicationController {

    //#region Constructor

    public constructor(@Inject(APPLICATION_SERVICE)
                       protected readonly _applicationService: IApplicationService,
                       @Inject(APPLICATION_VERSION_SERVICE)
                       protected readonly _applicationVersionService: IApplicationVersionService,
                       @Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
    }

    //#endregion

    //#region Methods

    @Post()
    @ApiOkResponse({
        description: 'Application successfully created',
        type: Application
    })
    @ApiOperation({
        description: 'Add an application'
    })
    @HttpCode(200)
    public async addAsync(@Body() model: AddApplicationViewModel): Promise<Application> {
        await this._validationService.validateAsync(addApplication, model, true);
        return await this._applicationService.addAsync(model);
    }

    @Put(':id')
    @ApiOperation({
        description: 'Edit an application'
    })
    @HttpCode(200)
    public async editAsync(@Param('id') id: string,
                           @Body() model: EditApplicationViewModel): Promise<Application> {
        await this._validationService.validateAsync(editApplication, model, true);
        return await this._applicationService.editAsync(id, model);
    }

    @Delete(':id')
    @ApiOperation({
        description: 'Delete an application'
    })
    @HttpCode(200)
    public deleteAsync(@Param('id') id: string): Promise<void> {
        return this._applicationService.deleteByIdAsync(id);
    }

    @Get()
    @HttpCode(200)
    @LenientAuthentication()
    public getAsync(): Promise<Application[]> {
        return this._applicationService.getAsync();
    }

    @Get(':id/version/:versionId')
    @HttpCode(200)
    @LenientAuthentication()
    public getApplicationVersionByIdAsync(
        @Param('id') applicationId: string,
        @Param('versionId') versionId: string): Promise<ApplicationVersion> {
        return this._applicationVersionService.getByIdAsync(versionId);
    }

    @Get(':id')
    @HttpCode(200)
    @LenientAuthentication()
    public getByIdAsync(@Param('id') id: string): Promise<Application> {
        return this._applicationService.getByIdAsync(id);
    }

    //#endregion

}
