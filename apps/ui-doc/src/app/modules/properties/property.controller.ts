import {Body, Controller, Delete, Get, HttpCode, Inject, Param, Post} from '@nestjs/common';
import {ApiProperty, ApiTags} from '@nestjs/swagger';
import {
    APPLICATION_SERVICE,
    COMPONENT_SERVICE,
    EVENT_SERVICE, METHOD_SERVICE,
    PROPERTY_SERVICE
} from '../../core-modules/database/constants/injectors';
import {IPropertyService} from '../../core-modules/database/services/interfaces/property-service.interface';
import {Property} from '../../core-modules/database/models/property';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {IApplicationService} from '../../core-modules/database/services/interfaces/application-service.interface';
import {IComponentService} from '../../core-modules/database/services/interfaces/component-service.interface';
import {IEventService} from '../../core-modules/database/services/interfaces/event-service.interface';
import {IMethodService} from '../../core-modules/database/services/interfaces/method-service.interface';
import {
    LenientAuthentication
} from '../../core-modules/services/authentication-service/decorators/authentication-metadata.decorator';
import {AddPropertyViewModel} from "../../../view-models/properties/add-property.view-model";
import {ItemNotFoundException} from "../../../models/exceptions/item-not-found.exception";
import {BusinessExceptionCodes} from "../../../constants/business-exception-codes";
import {BusinessExceptionMessages} from "../../../constants/business-exception-messages";
import {addProperty} from "../../../constants/validator-builders/property-validation-builders";

@ApiTags('property')
@Controller('api/property')
export class PropertyController {

    //#region Constructor

    public constructor(@Inject(PROPERTY_SERVICE)
                       protected readonly _propertyService: IPropertyService,
                       @Inject(APPLICATION_SERVICE)
                       protected readonly _applicationService: IApplicationService,
                       @Inject(COMPONENT_SERVICE)
                       protected readonly _componentService: IComponentService,
                       @Inject(EVENT_SERVICE)
                       protected readonly _eventService: IEventService,
                       @Inject(METHOD_SERVICE)
                       protected readonly _methodService: IMethodService,
                       @Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
    }

    //#endregion

    //#region Methods

    @Get(':id')
    @ApiProperty({
        description: 'Get a property by using its id.'
    })
    @HttpCode(200)
    @LenientAuthentication()
    public getByIdAsync(@Param('id') id: string): Promise<Property> {
        return this._propertyService.getByIdAsync(id);
    }

    @Post('to-component/:componentId')
    @ApiProperty({
        description: 'Add a property into a component.'
    })
    @HttpCode(200)
    public async addToComponentAsync(@Param('componentId') componentId: string,
                                     @Body() model: AddPropertyViewModel): Promise<Property> {

        // Find the component.
        const component = await this._componentService.getByIdAsync(componentId);
        if (!component) {
            throw new ItemNotFoundException(BusinessExceptionCodes.componentNotFound, BusinessExceptionMessages.componentNotFound);
        }

        await this._validationService.validateAsync(addProperty, model);
        return await this._propertyService.addToComponentAsync(componentId, model);
    }

    @Post('to-event/:eventId')
    @ApiProperty({
        description: 'Add a property into an event.'
    })
    @HttpCode(200)
    public async addToEventAsync(@Param('eventId') eventId: string,
                                 @Body() model: AddPropertyViewModel): Promise<Property> {

        await this._validationService.validateAsync(addProperty, model);

        const event = await this._eventService.getByIdAsync(eventId);
        if (!event) {
            throw new ItemNotFoundException(BusinessExceptionCodes.eventNotFound, BusinessExceptionMessages.eventNotFound);
        }

        return await this._propertyService.addToEventAsync(eventId, model);
    }

    @Post('to-method/:methodId')
    @ApiProperty({
        description: 'Add a property into a method.'
    })
    @HttpCode(200)
    public async addToMethodAsync(@Param('methodId') methodId: string,
                                  @Body() model: AddPropertyViewModel): Promise<Property> {

        await this._validationService.validateAsync(addProperty, model);

        const method = await this._methodService.getByIdAsync(methodId);
        if (!method) {
            throw new ItemNotFoundException(BusinessExceptionCodes.methodNotFound, BusinessExceptionMessages.methodNotFound);
        }

        return await this._propertyService.addToMethodAsync(methodId, model);
    }

    @Get('by-component/:componentId')
    @ApiProperty({
        description: 'Search for properties using component id.'
    })
    @HttpCode(200)
    @LenientAuthentication()
    public getByComponentIdAsync(@Param('componentId') componentId: string): Promise<Property[]> {
        return this._propertyService.getByComponentIdAsync(componentId);
    }

    @Get('by-event/:eventId')
    @ApiProperty({
        description: 'Search for properties using event id.'
    })
    @HttpCode(200)
    @LenientAuthentication()
    public getByEventIdAsync(@Param('eventId') eventId: string): Promise<Property[]> {
        return this._propertyService.getByEventIdAsync(eventId);
    }

    @Get('by-method/:methodId')
    @ApiProperty({
        description: 'Search for properties using method id.'
    })
    @HttpCode(200)
    @LenientAuthentication()
    public getByMethodIdAsync(@Param('methodId') methodId: string): Promise<Property[]> {
        return this._propertyService.getByMethodIdAsync(methodId);
    }

    @Delete(':id')
    @ApiProperty({
        description: 'Delete a property by using its id.'
    })
    @HttpCode(200)
    public deleteByIdAsync(@Param('id') id: string): Promise<void> {
        return this._propertyService.deleteByIdAsync(id);
    }

    //#endregion
}
