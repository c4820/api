import {Inject, Module} from '@nestjs/common';
import {DatabaseModule} from '../../core-modules/database/database.module';
import {PropertyController} from './property.controller';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {addPropertyValidatorBuilder} from "../../../constants/validator-builders/property-validation-builders";

@Module({
    imports: [
        DatabaseModule
    ],
    controllers: [
        PropertyController
    ]
})
export class PropertyModule {

    //#region Constructor

    public constructor(@Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
        this._validationService.with(addPropertyValidatorBuilder);
    }

    //#endregion

}
