import {Body, Controller, HttpCode, HttpStatus, Inject, Post} from '@nestjs/common';
import {
    IAuthenticationService
} from '../../core-modules/services/authentication-service/authentication-service.interface';
import {AUTHENTICATION_SERVICE} from '../../core-modules/services/authentication-service/constants/injectors';
import {
    IAuthenticationResult
} from '../../core-modules/services/authentication-service/interfaces/authentication-result.interface';
import {
    AllowAnonymous
} from '../../core-modules/services/authentication-service/decorators/authentication-metadata.decorator';
import {ApiOkResponse, ApiTags} from '@nestjs/swagger';
import {AuthenticationRequestViewModel} from "../../../view-models/authentication/authentication-request.view-model";

@ApiTags('open-authentication')
@Controller('oauth')
export class OpenAuthenticationController {

    //#region Constructor

    public constructor(
        @Inject(AUTHENTICATION_SERVICE)
        protected readonly _authenticationService: IAuthenticationService) {
    }

    //#endregion

    //#region Methods

    @ApiOkResponse({
        description: 'Exchange authorization code with access token & refresh token'
    })
    @Post('authorize')
    @HttpCode(200)
    @AllowAnonymous()
    public async authenticateAsync(@Body() command: AuthenticationRequestViewModel)
        : Promise<IAuthenticationResult> {
        return await this._authenticationService.authenticateAsync(command.code, command.redirectUri);
    }

    //#endregion
}
