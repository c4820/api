import {Module} from '@nestjs/common';
import {OpenAuthenticationController} from './open-authentication.controller';
import {
    AuthenticationServiceModule
} from '../../core-modules/services/authentication-service/authentication-service.module';

@Module({
    imports: [
        AuthenticationServiceModule
    ],
    controllers: [
        OpenAuthenticationController
    ]
})
export class OpenAuthenticationModule {

}
