import {Body, Controller, Delete, Get, HttpCode, Inject, Param, Post, Put, Query} from '@nestjs/common';
import {
    APPLICATION_SERVICE,
    APPLICATION_VERSION_SERVICE,
    COMPONENT_SERVICE
} from '../../core-modules/database/constants/injectors';
import {ApiOperation, ApiTags} from '@nestjs/swagger';
import {IApplicationService} from "../../core-modules/database/services/interfaces/application-service.interface";
import {IComponentService} from '../../core-modules/database/services/interfaces/component-service.interface';
import {Component} from '../../core-modules/database/models/component';
import {
    IApplicationVersionService
} from '../../core-modules/database/services/interfaces/application-version-service.interface';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {
    LenientAuthentication
} from '../../core-modules/services/authentication-service/decorators/authentication-metadata.decorator';
import {AddComponentViewModel} from "../../../view-models/components/add-component.view-model";
import {addComponent, editComponent} from "../../../constants/validator-builders/component-validation-builders";
import {ItemNotFoundException} from "../../../models/exceptions/item-not-found.exception";
import {BusinessExceptionCodes} from "../../../constants/business-exception-codes";
import {BusinessExceptionMessages} from "../../../constants/business-exception-messages";
import {EditComponentViewModel} from "../../../view-models/components/edit-component.view-model";

@ApiTags('component')
@Controller('api/component')
export class ComponentController {

    //#region Constructor

    public constructor(@Inject(APPLICATION_SERVICE)
                       protected readonly _applicationService: IApplicationService,
                       @Inject(APPLICATION_VERSION_SERVICE)
                       protected readonly _applicationVersionService: IApplicationVersionService,
                       @Inject(COMPONENT_SERVICE)
                       protected readonly _componentService: IComponentService,
                       @Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
    }

    //#endregion

    //#region Methods

    @ApiOperation({
        description: 'Get component by id'
    })
    @Get(':id')
    @HttpCode(200)
    @LenientAuthentication()
    public async getByIdAsync(@Param('id') id: string): Promise<Component> {
        return this._componentService.getByIdAsync(id);
    }

    @ApiOperation({
        description: 'Create a component'
    })
    @Post()
    @HttpCode(200)
    public async addAsync(@Body() model: AddComponentViewModel): Promise<Component> {

        // Do command validation.
        await this._validationService.validateAsync(addComponent, model, true);

        const application = await this._applicationService.getByIdAsync(model.applicationId);
        if (!application) {
            throw new ItemNotFoundException(BusinessExceptionCodes.applicationNotFound, BusinessExceptionMessages.applicationNotFound);
        }

        const version = await this._applicationVersionService.getByIdAsync(model.versionId);
        if (!version) {
            throw new ItemNotFoundException(BusinessExceptionCodes.versionNotFound, BusinessExceptionMessages.versionNotFound);
        }

        return await this._componentService.addAsync(model);
    }

    @Put(':id')
    @HttpCode(200)
    public async editAsync(@Param('id') id: string,
                           @Body() model: EditComponentViewModel): Promise<Component> {

        // Do validation.
        await this._validationService.validateAsync(editComponent, model, true);

        const version = await this._applicationVersionService.getByIdAsync(model.id);
        if (!version) {
            throw new ItemNotFoundException(BusinessExceptionCodes.versionNotFound, BusinessExceptionMessages.versionNotFound);
        }

        return await this._componentService.editAsync(model);
    }

    @Delete(':id')
    @HttpCode(200)
    public async deleteAsync(@Param('id') id: string): Promise<void> {
        return await this._componentService.deleteByIdAsync(id);
    }

    @ApiOperation({
        description: 'Get components by application & version'
    })
    @Get('by-application/:applicationId')
    @LenientAuthentication()
    @HttpCode(200)
    public async getAsync(@Param('applicationId') applicationId: string,
                          @Query('versionId') versionId: string): Promise<Component[]> {
        return await this._componentService.getByApplicationIdAsync(applicationId, versionId);
    }

    //#endregion
}
