import {Inject, Module} from '@nestjs/common';
import {DatabaseModule} from '../../core-modules/database/database.module';
import {ComponentController} from './component.controller';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {
  addComponentValidatorBuilder,
  editComponentValidatorBuilder
} from "../../../constants/validator-builders/component-validation-builders";

@Module({
    imports: [
        DatabaseModule
    ],
    controllers: [
        ComponentController
    ]
})
export class ComponentModule {

    //#region Constructor

    public constructor(@Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
        this._validationService.with(addComponentValidatorBuilder);
        this._validationService.with(editComponentValidatorBuilder);
    }

    //#endregion
}
