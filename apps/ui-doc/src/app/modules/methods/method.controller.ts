import {Body, Controller, Delete, Get, HttpCode, Inject, Param, Post, Put} from '@nestjs/common';
import {ApiOperation, ApiTags} from '@nestjs/swagger';
import {IMethodService} from '../../core-modules/database/services/interfaces/method-service.interface';
import {COMPONENT_SERVICE, METHOD_SERVICE} from '../../core-modules/database/constants/injectors';
import {Method} from '../../core-modules/database/models/method';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {IComponentService} from '../../core-modules/database/services/interfaces/component-service.interface';
import {
    LenientAuthentication
} from '../../core-modules/services/authentication-service/decorators/authentication-metadata.decorator';
import {BusinessExceptionMessages} from "../../../constants/business-exception-messages";
import {BusinessExceptionCodes} from "../../../constants/business-exception-codes";
import {ItemNotFoundException} from "../../../models/exceptions/item-not-found.exception";
import {addMethod, editMethod} from "../../../constants/validator-builders/method-validation-builders";
import {EditMethodViewModel} from "../../../view-models/methods/edit-method.view-model";
import {AddMethodViewModel} from "../../../view-models/methods/add-method.view-model";

@ApiTags('method')
@Controller('api/method')
export class MethodController {

    //#region Constructor

    public constructor(@Inject(METHOD_SERVICE)
                       protected readonly _methodService: IMethodService,
                       @Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService,
                       @Inject(COMPONENT_SERVICE)
                       protected readonly _componentService: IComponentService) {
    }

    //#endregion

    //#region Methods

    @Get(':id')
    @ApiOperation({
        description: 'Get method by search for its id.'
    })
    @HttpCode(200)
    @LenientAuthentication()
    public async getByIdAsync(@Param('id') id: string): Promise<Method> {
        return this._methodService.getByIdAsync(id);
    }

    @Get('by-component/:componentId')
    @ApiOperation({
        description: 'Search for methods which belong to a specific component'
    })
    @HttpCode(200)
    @LenientAuthentication()
    public async getByComponentIdAsync(@Param('componentId') componentId: string): Promise<Method[]> {
        return this._methodService.getByComponentIdAsync(componentId);
    }

    @Post()
    @ApiOperation({
        description: 'Add a method'
    })
    @HttpCode(200)
    public async addAsync(@Body() command: AddMethodViewModel): Promise<Method> {
        // Validate the command;
        await this._validationService.validateAsync(addMethod, command, true);

        // Find the component.
        const component = await this._componentService.getByIdAsync(command.componentId);
        if (!component) {
            throw new ItemNotFoundException(BusinessExceptionCodes.componentNotFound, BusinessExceptionMessages.componentNotFound);
        }

        return this._methodService.addAsync(command);
    }

    @Put(':componentId')
    @ApiOperation({
        description: 'Edit a specific method'
    })
    @HttpCode(200)
    public async editAsync(@Param('componentId') componentId: string,
                           @Body() command: EditMethodViewModel): Promise<Method> {
        // Validate the command.
        await this._validationService.validateAsync(editMethod, command, true);

        // Find the component.
        const component = await this._componentService.getByIdAsync(componentId);
        if (!component) {
            throw new ItemNotFoundException(BusinessExceptionCodes.componentNotFound, BusinessExceptionMessages.componentNotFound);
        }

        return this._methodService.editAsync(command);
    }

    @Delete(':id')
    @ApiOperation({
        description: 'Delete a method by searching for its id'
    })
    @HttpCode(200)
    public async deleteAsync(@Param('id') id: string): Promise<void> {
        await this._methodService.deleteAsync(id);
    }

    //#endregion
}
