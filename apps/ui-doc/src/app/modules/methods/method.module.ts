import {Inject, Module} from '@nestjs/common';
import {DatabaseModule} from '../../core-modules/database/database.module';
import {MethodController} from './method.controller';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {
  addMethodValidatorBuilder,
  editMethodValidatorBuilder
} from "../../../constants/validator-builders/method-validation-builders";

@Module({
    imports: [
        DatabaseModule
    ],
    controllers: [
        MethodController
    ]
})
export class MethodModule {

    //#region Constructor

    public constructor(@Inject(VALIDATION_SERVICE) protected readonly _validationService: IValidationService) {
        this._validationService.with(addMethodValidatorBuilder);
        this._validationService.with(editMethodValidatorBuilder);
    }

    //#endregion

}
