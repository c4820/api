import {Body, Controller, Delete, Get, Inject, Param, Post, Put} from '@nestjs/common';
import {Event} from '../../core-modules/database/models/event';
import {ApiOperation, ApiTags} from '@nestjs/swagger';
import {COMPONENT_SERVICE, EVENT_SERVICE} from '../../core-modules/database/constants/injectors';
import {IEventService} from '../../core-modules/database/services/interfaces/event-service.interface';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {IComponentService} from '../../core-modules/database/services/interfaces/component-service.interface';
import {merge as lodashMerge} from 'lodash';
import {
    LenientAuthentication
} from '../../core-modules/services/authentication-service/decorators/authentication-metadata.decorator';
import {AddEventViewModel} from "../../../view-models/events/add-event.view-model";
import {addEvent, editEvent} from "../../../constants/validator-builders/event-validation-builders";
import {ItemNotFoundException} from "../../../models/exceptions/item-not-found.exception";
import {BusinessExceptionCodes} from "../../../constants/business-exception-codes";
import {BusinessExceptionMessages} from "../../../constants/business-exception-messages";
import {EditEventViewModel} from "../../../view-models/events/edit-event.view-model";

@ApiTags('event')
@Controller('api/event')
export class EventController {

    //#region Constructor

    public constructor(@Inject(EVENT_SERVICE)
                       protected readonly _eventService: IEventService,
                       @Inject(COMPONENT_SERVICE)
                       protected readonly _componentService: IComponentService,
                       @Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
    }

    //#endregion

    //#region Methods

    @Get(':id')
    @LenientAuthentication()
    public async getByIdAsync(@Param('id') id: string): Promise<Event> {
        return this._eventService.getByIdAsync(id);
    }

    @Get('by-component/:componentId')
    @LenientAuthentication()
    public async getByComponentIdAsync(@Param('componentId') componentId: string): Promise<Event[]> {
        return this._eventService.getByComponentIdAsync(componentId);
    }

    @Post()
    public async addAsync(@Body() command: AddEventViewModel): Promise<Event> {
        // Do command validation.
        await this._validationService.validateAsync(addEvent, command, true);

        // Find the component.
        const component = await this._componentService.getByIdAsync(command.componentId);
        if (!component) {
            throw new ItemNotFoundException(BusinessExceptionCodes.componentNotFound, BusinessExceptionMessages.componentNotFound);
        }

        return this._eventService.addAsync(command);
    }

    @Put(':eventId')
    public async editAsync(@Param('eventId') id: string,
                           @Body() command: EditEventViewModel): Promise<Event> {

        const copiedCommand = lodashMerge(new EditEventViewModel(id), command);

        // Do command validation.
        await this._validationService.validateAsync(editEvent, copiedCommand, true);

        return this._eventService.editAsync(copiedCommand);
    }

    @Delete(':id')
    @ApiOperation({
        description: 'Delete an event by searching for its id.'
    })
    public async deleteByIdAsync(@Param('id') id: string): Promise<void> {
        await this._eventService.deleteByIdAsync(id);
    }

    //#endregion
}
