import {Inject, Module} from '@nestjs/common';
import {DatabaseModule} from '../../core-modules/database/database.module';
import {EventController} from './event.controller';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {
  addEventValidatorBuilder,
  editEventValidatorBuilder
} from "../../../constants/validator-builders/event-validation-builders";

@Module({
    imports: [
        DatabaseModule
    ],
    controllers: [
        EventController
    ]
})
export class EventModule {

    //#region Constructor

    public constructor(@Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
        this._validationService.with(addEventValidatorBuilder);
        this._validationService.with(editEventValidatorBuilder);
    }

    //#endregion

}
