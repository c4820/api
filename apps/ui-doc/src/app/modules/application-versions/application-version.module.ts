import {Inject, Module} from '@nestjs/common';
import {DatabaseModule} from '../../core-modules/database/database.module';
import {ApplicationVersionController} from './application-version.controller';
import {ValidatorModule} from '../../core-modules/validation/validation.module';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {
  addApplicationVersionValidatorBuilder, editApplicationVersionValidatorBuilder
} from "../../../constants/validator-builders/application-version-validation-builders";

@Module({
    imports: [
        DatabaseModule,
        ValidatorModule
    ],
    controllers: [
        ApplicationVersionController
    ],
    providers: []
})
export class ApplicationVersionModule {

    //#region Constructor

    public constructor(@Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
        this._validationService.with(addApplicationVersionValidatorBuilder);
        this._validationService.with(editApplicationVersionValidatorBuilder);
    }

    //#endregion
}
