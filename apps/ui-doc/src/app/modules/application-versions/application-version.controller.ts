import {Body, Controller, Delete, Get, HttpCode, Inject, Param, Post, Put} from '@nestjs/common';
import {APPLICATION_VERSION_SERVICE} from '../../core-modules/database/constants/injectors';
import {ApiTags} from '@nestjs/swagger';
import {ApplicationVersion} from '../../core-modules/database/models/application-version';
import {
    IApplicationVersionService
} from '../../core-modules/database/services/interfaces/application-version-service.interface';
import {VALIDATION_SERVICE} from '../../core-modules/validation/constants/injectors';
import {IValidationService} from '../../core-modules/validation/interfaces/validation-service.interface';
import {
    LenientAuthentication
} from '../../core-modules/services/authentication-service/decorators/authentication-metadata.decorator';
import {AddApplicationVersionViewModel} from "../../../view-models/applications/add-application-version.view-model";
import {
  addApplicationVersion,
  editApplicationVersion
} from "../../../constants/validator-builders/application-version-validation-builders";
import {EditApplicationVersionViewModel} from "../../../view-models/applications/edit-application-version.view-model";

@ApiTags('application-version')
@Controller('api/application-version')
export class ApplicationVersionController {

    //#region Constructor

    public constructor(@Inject(APPLICATION_VERSION_SERVICE)
                       protected readonly _applicationVersionService: IApplicationVersionService,
                       @Inject(VALIDATION_SERVICE)
                       protected readonly _validationService: IValidationService) {
    }

    //#endregion

    //#region Methods

    @Get(':versionId')
    @HttpCode(200)
    @LenientAuthentication()
    public getByIdAsync(@Param('versionId') versionId: string)
        : Promise<ApplicationVersion> {
        return this._applicationVersionService.getByIdAsync(versionId);
    }

    @Get('by-application-id/:applicationId')
    @HttpCode(200)
    @LenientAuthentication()
    public getByApplicationIdAsync(@Param('applicationId') applicationId: string)
        : Promise<ApplicationVersion[]> {
        return this._applicationVersionService.getByApplicationIdAsync(applicationId);
    }

    @Post('')
    @HttpCode(200)
    public async addAsync(@Body() command: AddApplicationVersionViewModel): Promise<ApplicationVersion> {
        await this._validationService.validateAsync(addApplicationVersion, command, true);
        return await this._applicationVersionService.addAsync(command);
    }

    @Put(':id')
    @HttpCode(200)
    public async editAsync(
        @Param('id') id: string,
        @Body() command: EditApplicationVersionViewModel): Promise<ApplicationVersion> {
        await this._validationService.validateAsync(editApplicationVersion, command, true);
        return this._applicationVersionService.editAsync(id, command);
    }

    @Delete(':id')
    @HttpCode(200)
    public async deleteByIdAsync(@Param('id') id: string): Promise<void> {
        await this._applicationVersionService.deleteByIdAsync(id);
    }

    //#endregion

}
