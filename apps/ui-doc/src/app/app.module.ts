import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {DatabaseModule} from './core-modules/database/database.module';
import {ComponentModule} from './modules/components/component.module';
import {EventModule} from './modules/events/event.module';
import {ApplicationModule} from './modules/applications/application.module';
import {ConfigModule} from '@nestjs/config';
import {MethodModule} from './modules/methods/method.module';
import {PropertyModule} from './modules/properties/property.module';
import {ApplicationVersionModule} from './modules/application-versions/application-version.module';
import {ValidatorModule} from './core-modules/validation/validation.module';
import {OpenAuthenticationModule} from './modules/open-authentication/open-authentication.module';
import {AuthenticationModule} from './core-modules/authentication/authentication.module';
import {ExampleModule} from './modules/examples/example.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
            envFilePath: ['.env', '.env.development', '.env.production']
        }),
        ValidatorModule.forRoot(),
        DatabaseModule,

        AuthenticationModule,
        OpenAuthenticationModule,
        ApplicationModule,
        ApplicationVersionModule,
        ComponentModule,
        EventModule,
        ExampleModule,
        MethodModule,
        PropertyModule
    ],
    providers: []
})
export class AppModule {
}
