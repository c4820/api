import {Module, Scope} from '@nestjs/common';
import {
    APPLICATION_SERVICE, APPLICATION_VERSION_SERVICE, COMPONENT_SERVICE,
    DATABASE_CONTEXT, EVENT_SERVICE, EXAMPLE_SERVICE, METHOD_SERVICE, PROPERTY_SERVICE,
} from './constants/injectors';
import {auth, driver} from 'neo4j-driver';
import {ApplicationService} from './services/implementations/application.service';
import {ComponentService} from './services/implementations/component.service';
import {EventService} from './services/implementations/event.service';
import {MethodService} from './services/implementations/method.service';
import {TimeContextModule} from '../services/time-context/time-context.module';
import {ConfigModule, ConfigService} from '@nestjs/config';
import {PropertyService} from './services/implementations/property.service';
import {ApplicationVersionService} from './services/implementations/application-version.service';
import {ExampleService} from './services/implementations/example.service';
import {DatabaseConfigurationKeys} from "../../../constants/configuration-keys/database-configuration-keys";

@Module({
    imports: [
        TimeContextModule,
        ConfigModule
    ],
    providers: [
        {
            provide: DATABASE_CONTEXT,
            useFactory: (configService: ConfigService) => {
                const databaseUri = configService.get<string>(DatabaseConfigurationKeys.databaseUri);
                const username = configService.get<string>(DatabaseConfigurationKeys.databaseUsername);
                const password = configService.get<string>(DatabaseConfigurationKeys.databasePassword);
                return driver(databaseUri, auth.basic(username, password),
                    {disableLosslessIntegers: true});
            },
            scope: Scope.REQUEST,
            inject: [ConfigService]
        },
        {
            provide: APPLICATION_SERVICE,
            useClass: ApplicationService,
            scope: Scope.REQUEST
        },
        {
            provide: COMPONENT_SERVICE,
            useClass: ComponentService,
            scope: Scope.REQUEST
        },
        {
            provide: EVENT_SERVICE,
            useClass: EventService,
            scope: Scope.REQUEST
        },
        {
            provide: METHOD_SERVICE,
            useClass: MethodService,
            scope: Scope.REQUEST
        },
        {
            provide: PROPERTY_SERVICE,
            useClass: PropertyService,
            scope: Scope.REQUEST
        },
        {
            provide: APPLICATION_VERSION_SERVICE,
            useClass: ApplicationVersionService,
            scope: Scope.REQUEST
        },
        {
            provide: EXAMPLE_SERVICE,
            useClass: ExampleService,
            scope: Scope.REQUEST
        }
    ],
    exports: [
        DATABASE_CONTEXT,
        APPLICATION_SERVICE,
        COMPONENT_SERVICE,
        EVENT_SERVICE,
        EXAMPLE_SERVICE,
        METHOD_SERVICE,
        PROPERTY_SERVICE,
        APPLICATION_VERSION_SERVICE
    ]
})
export class DatabaseModule {

}
