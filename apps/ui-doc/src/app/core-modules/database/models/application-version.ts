export class ApplicationVersion {

    //#region Properties

    public title: string;

    public description?: string;

    public changeLogs?: string[];

    //#endregion

    //#region Constructor

    public constructor(public readonly id: string) {
    }

    //#endregion

}
