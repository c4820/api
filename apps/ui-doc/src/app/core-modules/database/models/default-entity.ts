import {ItemAvailabilities} from "../../../../enumerations/item-availabilities";


export abstract class DefaultEntity {

    //#region Properties

    public availability: ItemAvailabilities;

    public createdTime: number;

    public lastModifiedTime: number;

    //#endregion

    //#region Constructor

    protected constructor() {
        this.availability = ItemAvailabilities.available;
    }

    //#endregion

}
