import {DefaultEntity} from './default-entity';

export class Example extends DefaultEntity {

    //#region Constructor

    public constructor(public readonly id: string,
                       public readonly title: string,
                       public readonly description: string) {
        super();
    }

    //#endregion

}
