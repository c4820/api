import {DefaultEntity} from './default-entity';

export class Property extends DefaultEntity {

    //#region Properties

    public name: string;

    public description: string;

    public valueType: string;

    public defaultValue: string;

    public optional: boolean;

    //#endregion

    //#region Constructor

    public constructor(public readonly id: string) {
        super();
    }

    //#endregion
}
