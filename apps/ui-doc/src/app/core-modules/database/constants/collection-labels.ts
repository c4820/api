export class CollectionLabels {

    //#region Properties

    public static readonly application = 'Application';

    public static readonly component = 'Component';

    public static readonly property = 'Property';

    public static readonly method = 'Method';

    public static readonly event = 'Event';

    public static readonly applicationVersion = 'ApplicationVersion';

    public static readonly example = 'Example';

    //#endregion

}
