export class EntityRelationships {

    //#region Properties

    public static readonly hasComponent = 'HAS_COMPONENT';

    public static readonly hasEvent = 'HAS_EVENT';

    public static readonly hasMethod = 'HAS_METHOD';

    public static readonly hasProperty = 'HAS_PROPERTY';

    public static readonly hasVersion = 'HAS_VERSION';

    public static readonly hasExample = 'HAS_EXAMPLE';

    //#endregion

}
