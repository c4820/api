import {IApplicationVersionService} from '../interfaces/application-version-service.interface';
import {Inject, Injectable} from '@nestjs/common';
import {ApplicationVersion} from '../../models/application-version';
import {merge as lodashMerge} from 'lodash';
import {CollectionLabels} from '../../constants/collection-labels';
import {DATABASE_CONTEXT} from '../../constants/injectors';
import {Driver} from 'neo4j-driver';
import {TIME_CONTEXT} from '../../../services/time-context/injectors';
import {ITimeContext} from '../../../services/time-context/time-context.interface';
import {randomUUID} from 'crypto';
import {EntityRelationships} from '../../constants/entity-relationships';
import {
  AddApplicationVersionViewModel
} from "../../../../../view-models/applications/add-application-version.view-model";
import {
  EditApplicationVersionViewModel
} from "../../../../../view-models/applications/edit-application-version.view-model";

@Injectable()
export class ApplicationVersionService implements IApplicationVersionService {

    //#region Constructor

    public constructor(@Inject(DATABASE_CONTEXT)
                       protected readonly _databaseContext: Driver,
                       @Inject(TIME_CONTEXT)
                       protected readonly _timeContext: ITimeContext) {
    }

    //#endregion

    //#region Methods

    public async addAsync(model: AddApplicationVersionViewModel): Promise<ApplicationVersion> {
        const id = randomUUID();
        const applicationVersion = new ApplicationVersion(id);
        applicationVersion.title = model.title;
        applicationVersion.description = model.description;

        const szQuery = `
        OPTIONAL MATCH (application:${CollectionLabels.application}) WHERE application.id = '${model.applicationId}' WITH application
        OPTIONAL MATCH (application)-[:${EntityRelationships.hasVersion}]->(version:${CollectionLabels.applicationVersion}) WHERE version.title = '${model.title}' WITH application,version,
        CASE WHEN EXISTS(application.id) AND EXISTS(version.id) THEN 2
	    WHEN EXISTS(application.id) THEN 1
	    ELSE 0
        END AS OUTPUT
        FOREACH(temp in CASE WHEN OUTPUT=1 then [1] ELSE [] END |
	        MERGE (application)-[:${EntityRelationships.hasVersion}]->(addedVersion:ApplicationVersion{id: '${id}'} ) ON CREATE SET addedVersion += $instance
        )
        FOREACH(temp in CASE WHEN OUTPUT=0 then [1] ELSE [] END |
            CREATE (application)-[:${EntityRelationships.hasVersion}]->(version)
        );`;

        const session = this._databaseContext.session();
        await session.run(szQuery, {
            instance: applicationVersion
        })

        await session.close();
        return applicationVersion;
    }

    public async editAsync(versionId: string, model: EditApplicationVersionViewModel): Promise<ApplicationVersion> {
        const actualModel = lodashMerge(new EditApplicationVersionViewModel(), model);
        const updatedParams = {};
        for (const key of Object.keys(actualModel)) {
            if (!actualModel[key]?.hasModified) {
                continue;
            }

            updatedParams[key] = actualModel[key].value;
        }

        const szQuery = `MATCH (version: ${CollectionLabels.applicationVersion}) WHERE version.id = '${versionId}'
                         SET version += $params RETURN version`;

        const session = this._databaseContext.session();
        const commandResult = await session.run(szQuery, {
            params: updatedParams
        })

        await session.close();
        return lodashMerge(new ApplicationVersion(null), commandResult.records[0]?.get('version').properties);
    }

    public async deleteByIdAsync(versionId: string): Promise<void> {
        const szQuery = `MATCH (version: ${CollectionLabels.applicationVersion}) WHERE version.id = '${versionId}' DETACH DELETE version`;

        const session = this._databaseContext.session();
        await session.run(szQuery)

        await session.close();
    }

    public async getByApplicationIdAsync(applicationId: string): Promise<ApplicationVersion[]> {
        const session = this._databaseContext.session();
        const command = `MATCH (application:${CollectionLabels.application})-[:${EntityRelationships.hasVersion}]->(version:${CollectionLabels.applicationVersion}) WHERE application.id = '${applicationId}' RETURN version`;

        const queryResult = await session.run(command);
        await session.close();

        const items = queryResult.records.map(x => lodashMerge(new ApplicationVersion(null), x.get('version').properties));
        return items;
    }

    public async getByIdAsync(versionId: string): Promise<ApplicationVersion> {
        const session = this._databaseContext.session();
        const command = `MATCH (application:${CollectionLabels.application})-[:${EntityRelationships.hasVersion}]->(version:${CollectionLabels.applicationVersion})
        WHERE version.id = '${versionId}' RETURN version`;

        const queryResult = await session.run(command);
        await session.close();

        const items = queryResult.records.map(x => lodashMerge(new ApplicationVersion(null), x.get('version').properties));
        return items[0];
    }

    //#endregion
}
