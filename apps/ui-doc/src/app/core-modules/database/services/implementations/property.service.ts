import {IPropertyService} from '../interfaces/property-service.interface';
import {Inject, Injectable} from '@nestjs/common';
import {Property} from '../../models/property';
import {DATABASE_CONTEXT} from '../../constants/injectors';
import {Driver} from 'neo4j-driver';
import {TIME_CONTEXT} from '../../../services/time-context/injectors';
import {ITimeContext} from '../../../services/time-context/time-context.interface';
import {randomUUID} from 'crypto';
import {CollectionLabels} from '../../constants/collection-labels';
import {merge as lodashMerge} from 'lodash';
import {EntityRelationships} from '../../constants/entity-relationships';
import {AddPropertyViewModel} from "../../../../../view-models/properties/add-property.view-model";

@Injectable()
export class PropertyService implements IPropertyService {

    //#region Constructor

    public constructor(@Inject(DATABASE_CONTEXT)
                       protected readonly _databaseContext: Driver,
                       @Inject(TIME_CONTEXT)
                       protected readonly _timeContext: ITimeContext) {
    }

    //#endregion

    //#region Methods

    public async addToComponentAsync(componentId: string, model: AddPropertyViewModel): Promise<Property> {
        const id = randomUUID();
        const property = new Property(id);
        property.name = model.name;
        property.description = model.description;
        property.valueType = model.valueType;
        property.defaultValue = model.defaultValue;
        property.optional = model.optional;

        const session = this._databaseContext.session();
        const szQuery = `MATCH (component:${CollectionLabels.component}) WHERE component.id = '${componentId}'
                        CREATE (component)-[:${EntityRelationships.hasProperty}]->(property:${CollectionLabels.property} $instance)
                        RETURN property`;

        const result = await session.run(szQuery, {
            instance: property
        });

        await session.close();
        const item = result.records[0].get('property').properties;
        return lodashMerge(new Property(id), item);
    }

    public async addToEventAsync(eventId: string, model: AddPropertyViewModel): Promise<Property> {
        const id = randomUUID();
        const property = new Property(id);
        property.name = model.name;
        property.description = model.description;
        property.valueType = model.valueType;
        property.defaultValue = model.defaultValue;
        property.optional = model.optional;

        const session = this._databaseContext.session();
        const szQuery = `MATCH (event:${CollectionLabels.event}) WHERE event.id = '${eventId}'
                        CREATE (event)-[:${EntityRelationships.hasProperty}]->(property:${CollectionLabels.property} $instance)
                        RETURN property`;

        const result = await session.run(szQuery, {
            instance: property
        });

        await session.close();
        const item = result.records[0].get('property').properties;
        return lodashMerge(new Property(id), item);
    }

    public async addToMethodAsync(methodId: string, model: AddPropertyViewModel): Promise<Property> {
        const id = randomUUID();
        const property = new Property(id);
        property.name = model.name;
        property.description = model.description;
        property.valueType = model.valueType;
        property.defaultValue = model.defaultValue;
        property.optional = model.optional;

        const session = this._databaseContext.session();
        const szQuery = `MATCH (method:${CollectionLabels.method}) WHERE method.id = '${methodId}'
                        CREATE (method)-[:${EntityRelationships.hasProperty}]->(property:${CollectionLabels.property} $instance)
                        RETURN property`;

        const result = await session.run(szQuery, {
            instance: property
        });

        await session.close();
        const item = result.records[0].get('property').properties;
        return lodashMerge(new Property(id), item);
    }

    public async getByIdAsync(id: string): Promise<Property> {
        const szQuery = `MATCH (property:${CollectionLabels.property}) WHERE property.id = '${id}' RETURN property`;
        const session = this._databaseContext.session();
        const result = await session.run(szQuery);

        await session.close();
        return result.records[0].get('property').properties;
    }

    public async getByComponentIdAsync(componentId: string): Promise<Property[]> {
        const szQuery = `MATCH (component:${CollectionLabels.component})-[:${EntityRelationships.hasProperty}]->(property:${CollectionLabels.property}) WHERE component.id = '${componentId}' RETURN property`;
        const session = this._databaseContext.session();
        const result = await session.run(szQuery);

        await session.close();
        const items = result.records.map(x => x.get('property').properties);
        return items;
    }

    public async getByEventIdAsync(eventId: string): Promise<Property[]> {
        const szQuery = `MATCH (event:${CollectionLabels.event})-[:${EntityRelationships.hasProperty}]->(property:${CollectionLabels.property}) WHERE event.id = '${eventId}' RETURN property`;
        const session = this._databaseContext.session();
        const result = await session.run(szQuery);

        await session.close();
        const items = result.records.map(x => x.get('property').properties);
        return items;
    }

    public async getByMethodIdAsync(methodId: string): Promise<Property[]> {
        const szQuery = `MATCH (method:${CollectionLabels.method})-[:${EntityRelationships.hasProperty}]->(property:${CollectionLabels.property}) WHERE method.id = '${methodId}' RETURN property`;
        const session = this._databaseContext.session();
        const result = await session.run(szQuery);

        await session.close();
        const items = result.records.map(x => x.get('property').properties);
        return items;
    }

    public async deleteByIdAsync(id: string): Promise<void> {
        const szQuery = `MATCH (property:${CollectionLabels.property}) WHERE property.id = '${id}' DETACH DELETE property`;
        const session = this._databaseContext.session();

        await session.run(szQuery);
        await session.close();
    }

    //#endregion

}
