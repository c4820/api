import {IApplicationService} from '../interfaces/application-service.interface';
import {Inject, Injectable} from '@nestjs/common';
import {Application} from '../../models/application';
import {DATABASE_CONTEXT} from '../../constants/injectors';
import {Driver, int} from 'neo4j-driver';
import {CollectionLabels} from '../../constants/collection-labels';
import {randomUUID} from 'crypto';
import {merge as lodashMerge} from 'lodash';
import {ItemAvailabilities} from "../../../../../enumerations/item-availabilities";
import {EditApplicationViewModel} from "../../../../../view-models/applications/edit-application.view-model";
import {AddApplicationViewModel} from "../../../../../view-models/applications/add-application.view-model";

@Injectable()
export class ApplicationService implements IApplicationService {

    //#region Constructor

    public constructor(@Inject(DATABASE_CONTEXT) protected readonly _databaseContext: Driver) {
    }

    //#endregion

    //#region Methods

    public async addAsync(model: AddApplicationViewModel): Promise<Application> {
        const id = randomUUID();
        const application = new Application(id);
        application.name = model.name;
        application.description = model.description;

        const session = this._databaseContext.session();
        const result = await session.run(`CREATE (application: ${CollectionLabels.application} $instance) RETURN application`, {
            instance: application
        });
        await session.close();

        const item = result.records[0].get('application').properties;
        return lodashMerge(new Application(id), item);
    }

    public async getByIdAsync(id: string): Promise<Application> {
        const session = this._databaseContext.session();
        const command = `MATCH (application:${CollectionLabels.application}) WHERE application.id = '${id}' RETURN application as item SKIP 0 LIMIT 1`;
        const queryResult = await session.run(command);
        await session.close();

        const item = queryResult.records[0].get('item').properties;
        return lodashMerge(new Application(null), item);
    }

    public async changeAvailabilityAsync(id: string, availability: ItemAvailabilities): Promise<void> {
        const session = this._databaseContext.session();
        await session.run(`MATCH (n: ${CollectionLabels.application})-[*0..]->(child)
        WHERE n.id = $applicationId
        SET child.availability = $availability `, {
            applicationId: id,
            availability: int(availability)
        });

        return void (0);
    }

    public async editAsync(id: string, model: EditApplicationViewModel): Promise<Application> {
        const actualModel = lodashMerge(new EditApplicationViewModel(), model);
        const updatedParams = {};
        for (const key of Object.keys(actualModel)) {
            if (!actualModel[key]?.hasModified) {
                continue;
            }

            updatedParams[key] = actualModel[key].value;
        }

        const session = this._databaseContext.session();
        const updateResult = await session.run(
            `MATCH (n: ${CollectionLabels.application} {id: $applicationId}) SET n += $params RETURN n{.*} as item`,
            {
                applicationId: id,
                params: updatedParams
            });

        return lodashMerge(new Application(null), updateResult.records[0]?.get('item'));
    }

    public async getAsync(): Promise<Application[]> {
        const command = `MATCH (n:${CollectionLabels.application})
         RETURN n{.*} as item`;

        const session = this._databaseContext.session();
        const queryResult = await session.run(command);
        await session.close();
        return queryResult.records
            .map(record => lodashMerge(new Application(null), record.get('item')));
    }

    public async deleteByIdAsync(id: string): Promise<void> {
        const command = `MATCH (application:${CollectionLabels.application}) WHERE application.id = '${id}' DETACH DELETE application`;

        const session = this._databaseContext.session();
        await session.run(command);
        await session.close();
    }

    //#endregion
}
