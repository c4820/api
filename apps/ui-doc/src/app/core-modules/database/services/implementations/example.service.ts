import {IExampleService} from '../interfaces/example-service.interface';
import {Inject, Injectable} from '@nestjs/common';
import {Example} from '../../models/example';
import {DATABASE_CONTEXT} from '../../constants/injectors';
import {Driver} from 'neo4j-driver';
import {randomUUID} from 'crypto';
import {CollectionLabels} from '../../constants/collection-labels';
import {EntityRelationships} from '../../constants/entity-relationships';
import {merge as lodashMerge} from 'lodash';
import {TIME_CONTEXT} from '../../../services/time-context/injectors';
import {ITimeContext} from '../../../services/time-context/time-context.interface';
import {AddExampleViewModel} from "../../../../../view-models/examples/add-example.view-model";
import {EditExampleViewModel} from "../../../../../view-models/examples/edit-example.view-model";

@Injectable()
export class ExampleService implements IExampleService {

    //#region Constructor

    public constructor(@Inject(DATABASE_CONTEXT)
                       protected readonly _databaseContext: Driver,
                       @Inject(TIME_CONTEXT)
                       protected readonly _timeContext: ITimeContext) {
    }

    //#endregion

    //#region Methods

    public async addAsync(command: AddExampleViewModel): Promise<Example> {
        const id = randomUUID();
        const example = new Example(id, command.title, command.description);
        example.createdTime = this._timeContext.getDate().getTime();

        const session = this._databaseContext.session();
        const szQuery = `MATCH (component:${CollectionLabels.component}) WHERE component.id = '${command.componentId}'
                        CREATE (component)-[:${EntityRelationships.hasExample}]->(example:${CollectionLabels.example} $instance)
                        RETURN example`;
        const result = await session.run(szQuery, {
            instance: example
        });
        await session.close();
        const item = result.records[0].get('example').properties;
        return lodashMerge(new Example(id, '', ''), item);
    }

    public async editAsync(command: EditExampleViewModel): Promise<Example> {

        const actualModel = lodashMerge(new EditExampleViewModel(command.id), command);
        const updatedParams = {} as Example;
        for (const key of Object.keys(actualModel)) {
            if (!actualModel[key]?.hasModified) {
                continue;
            }

            updatedParams[key] = actualModel[key].value;
        }
        updatedParams.lastModifiedTime = this._timeContext.getDate().getTime();

        const session = this._databaseContext.session();
        const result = await session.run(
            `MATCH (example: ${CollectionLabels.example} {id: $id}) SET example += $params RETURN example{.*} as item`,
            {
                id: command.id,
                params: updatedParams
            });
        await session.close();
        const item = result.records[0].get('item').properties;
        return lodashMerge(new Example(command.id, '', ''), item);
    }

    public async getByComponentIdAsync(componentId: string): Promise<Example[]> {
        const session = this._databaseContext.session();
        const szQuery = `MATCH (component:${CollectionLabels.component})-[:${EntityRelationships.hasExample}]->(example:${CollectionLabels.example})
                        WHERE component.id = '${componentId}'
                        RETURN example`;
        const queryResult = await session.run(szQuery);
        await session.close();

        const items = queryResult.records.map(x => {
            const properties = x.get('example').properties;
            return lodashMerge(new Example('', '', ''), properties);
        });

        return items;
    }

    public async getByIdAsync(id: string): Promise<Example> {
        const session = this._databaseContext.session();
        const result = await session.run(
            `MATCH (example: ${CollectionLabels.example}) RETURN example`);
        await session.close();
        const item = result.records[0].get('example').properties;
        return lodashMerge(new Example('', '', ''), item);
    }

    public async deleteByIdAsync(id: string): Promise<void> {
        const session = this._databaseContext.session();
        await session.run(
            `MATCH (example: ${CollectionLabels.example}) where example.id = '${id}' DETACH DELETE example`);
        await session.close();
    }

    //#endregion


}
