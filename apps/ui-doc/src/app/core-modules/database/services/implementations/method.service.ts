import {IMethodService} from '../interfaces/method-service.interface';
import {Inject, Injectable} from '@nestjs/common';
import {DATABASE_CONTEXT} from '../../constants/injectors';
import {Driver, int} from 'neo4j-driver';
import {TIME_CONTEXT} from '../../../services/time-context/injectors';
import {ITimeContext} from '../../../services/time-context/time-context.interface';
import {Method} from '../../models/method';
import {randomUUID} from 'crypto';
import {CollectionLabels} from '../../constants/collection-labels';
import {merge as lodashMerge} from 'lodash';
import {EntityRelationships} from '../../constants/entity-relationships';
import {AddMethodViewModel} from "../../../../../view-models/methods/add-method.view-model";
import {ItemAvailabilities} from "../../../../../enumerations/item-availabilities";
import {EditMethodViewModel} from "../../../../../view-models/methods/edit-method.view-model";

@Injectable()
export class MethodService implements IMethodService {

    //#region Constructor

    public constructor(@Inject(DATABASE_CONTEXT)
                       protected readonly _databaseContext: Driver,
                       @Inject(TIME_CONTEXT)
                       protected readonly _timeContext: ITimeContext) {
    }

    //#endregion

    //#region Methods

    public async addAsync(model: AddMethodViewModel): Promise<Method> {
        const id = randomUUID();
        const method = new Method(id);
        method.name = model.name;
        method.valueType = model.valueType;
        method.description = model.description;
        method.availability = model.availability;
        method.createdTime = this._timeContext.getDate().getTime();

        const session = this._databaseContext.session();
        const szQuery = `MATCH (component:${CollectionLabels.component}) WHERE component.id = '${model.componentId}'
                        CREATE (component)-[:${EntityRelationships.hasMethod}]->(method:${CollectionLabels.method} $instance)
                        RETURN method`;
        const result = await session.run(szQuery, {
            instance: method
        });

        await session.close();
        const item = result.records[0].get('method').properties;
        return lodashMerge(new Method(id), item);
    }

    public async changeAvailabilityAsync(id: string, availability: ItemAvailabilities): Promise<void> {
        const session = this._databaseContext.session();
        await session.run(`MATCH (n: ${CollectionLabels.method})
        WHERE n.id = $id
        SET n.availability = $availability `, {
            id: id,
            availability: int(availability)
        });

        return void (0);
    }

    public async editAsync(model: EditMethodViewModel): Promise<Method> {
        const actualModel = lodashMerge(new EditMethodViewModel(model.id), model);
        const updatedParams = {};
        for (const key of Object.keys(actualModel)) {
            if (!actualModel[key]?.hasModified) {
                continue;
            }

            updatedParams[key] = actualModel[key].value;
        }

        const session = this._databaseContext.session();
        const updateResult = await session.run(
            `MATCH (n: ${CollectionLabels.method} {id: $id}) SET n += $params RETURN n{.*} as item`,
            {
                id: model.id,
                params: updatedParams
            });

        return lodashMerge(new Method(null), updateResult.records[0]?.get('item'));
    }

    public async getByComponentIdAsync(componentId: string): Promise<Method[]> {
        const session = this._databaseContext.session();
        const command = `MATCH (component:${CollectionLabels.component})-[:${EntityRelationships.hasMethod}]->(method:${CollectionLabels.method}) WHERE component.id = '${componentId}' RETURN method`;
        const queryResult = await session.run(command);
        await session.close();

        return queryResult.records.map(x => lodashMerge(new Method(null), x.get('method').properties));
    }

    public async getByIdAsync(id: string): Promise<Method> {
        const session = this._databaseContext.session();
        const command = `MATCH (method:${CollectionLabels.method}) WHERE method.id = '${id}' RETURN method as item SKIP 0 LIMIT 1`;
        const queryResult = await session.run(command);
        await session.close();
        const item = queryResult.records[0].get('item').properties;
        return lodashMerge(new Method(null), item);
    }

    public async deleteAsync(id: string): Promise<void> {
        const session = this._databaseContext.session();
        const command = `MATCH (method:${CollectionLabels.method}) WHERE method.id = '${id}' DETACH DELETE method`;
        await session.run(command);
        await session.close();
    }

    //#endregion
}
