import {IEventService} from '../interfaces/event-service.interface';
import {Inject, Injectable} from '@nestjs/common';
import {DATABASE_CONTEXT} from '../../constants/injectors';
import {Driver, int} from 'neo4j-driver';
import {TIME_CONTEXT} from '../../../services/time-context/injectors';
import {ITimeContext} from '../../../services/time-context/time-context.interface';
import {randomUUID} from 'crypto';
import {CollectionLabels} from '../../constants/collection-labels';
import {merge as lodashMerge} from 'lodash';
import {Event} from '../../models/event';
import {EntityRelationships} from '../../constants/entity-relationships';
import {AddEventViewModel} from "../../../../../view-models/events/add-event.view-model";
import {ItemAvailabilities} from "../../../../../enumerations/item-availabilities";
import {EditEventViewModel} from "../../../../../view-models/events/edit-event.view-model";

@Injectable()
export class EventService implements IEventService {

    //#region Constructor

    public constructor(@Inject(DATABASE_CONTEXT)
                       protected readonly _databaseContext: Driver,
                       @Inject(TIME_CONTEXT)
                       protected readonly _timeContext: ITimeContext) {
    }

    //#endregion

    //#region Methods

    public async addAsync(model: AddEventViewModel): Promise<Event> {
        const id = randomUUID();
        const event = new Event(id);
        event.name = model.name;
        event.description = model.description;
        event.availability = model.availability;
        event.createdTime = this._timeContext.getDate().getTime();

        const session = this._databaseContext.session();
        const szQuery = `MATCH (component:${CollectionLabels.component}) WHERE component.id = '${model.componentId}'
                        CREATE (component)-[:${EntityRelationships.hasEvent}]->(event:${CollectionLabels.event} $instance)
                        RETURN event`;
        const result = await session.run(szQuery, {
            instance: event
        });
        await session.close();

        const item = result.records[0].get('event').properties;
        return lodashMerge(new Event(id), item);
    }

    public async deleteByIdAsync(eventId: string): Promise<void> {

        const session = this._databaseContext.session();
        const szQuery = `MATCH (event:${CollectionLabels.event}) WHERE event.id = '${eventId}' DETACH DELETE event`;
        await session.run(szQuery);
        await session.close();
    }

    public async changeAvailabilityAsync(id: string, availability: ItemAvailabilities): Promise<void> {
        const session = this._databaseContext.session();
        await session.run(`MATCH (n: ${CollectionLabels.event})
        WHERE n.id = $id
        SET n.availability = $availability `, {
            id: id,
            availability: int(availability)
        });

        return void (0);
    }

    public async editAsync(model: EditEventViewModel): Promise<Event> {
        const actualModel = lodashMerge(new EditEventViewModel(model.id), model);
        const updatedParams = {};
        for (const key of Object.keys(actualModel)) {
            if (!actualModel[key]?.hasModified) {
                continue;
            }

            updatedParams[key] = actualModel[key].value;
        }

        const session = this._databaseContext.session();
        const updateResult = await session.run(
            `MATCH (n: ${CollectionLabels.event} {id: $eventId}) SET n += $params RETURN n{.*} as item`,
            {
                eventId: model.id,
                params: updatedParams
            });

        return lodashMerge(new Event(null), updateResult.records[0]?.get('item'));
    }

    public async getByComponentIdAsync(componentId: string): Promise<Event[]> {
        const session = this._databaseContext.session();
        const command = `MATCH (component:${CollectionLabels.component})-[:${EntityRelationships.hasEvent}]->(event:${CollectionLabels.event}) WHERE component.id = '${componentId}' RETURN event`;
        const queryResult = await session.run(command);
        await session.close();

        const items = queryResult.records.map(x => x.get('event').properties);
        return items;
    }

    public async getByIdAsync(id: string): Promise<Event> {
        const session = this._databaseContext.session();
        const command = `MATCH (event:${CollectionLabels.event}) WHERE event.id = '${id}' RETURN event SKIP 0 LIMIT 1`;
        const queryResult = await session.run(command);
        await session.close();
        const item = queryResult.records[0].get('event').properties;
        return lodashMerge(new Event(null), item);
    }

    //#endregion

}
