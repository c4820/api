import {IComponentService} from '../interfaces/component-service.interface';
import {Inject, Injectable} from '@nestjs/common';
import {Component} from '../../models/component';
import {DATABASE_CONTEXT} from '../../constants/injectors';
import {Driver, int} from 'neo4j-driver';
import {randomUUID} from 'crypto';
import {CollectionLabels} from '../../constants/collection-labels';
import {merge as lodashMerge} from 'lodash';
import {TIME_CONTEXT} from '../../../services/time-context/injectors';
import {ITimeContext} from '../../../services/time-context/time-context.interface';
import {EntityRelationships} from '../../constants/entity-relationships';
import {AddComponentViewModel} from "../../../../../view-models/components/add-component.view-model";
import {ItemAvailabilities} from "../../../../../enumerations/item-availabilities";
import {EditComponentViewModel} from "../../../../../view-models/components/edit-component.view-model";

@Injectable()
export class ComponentService implements IComponentService {

    //#region Constructor

    public constructor(@Inject(DATABASE_CONTEXT)
                       protected readonly _databaseContext: Driver,
                       @Inject(TIME_CONTEXT)
                       protected readonly _timeContext: ITimeContext) {
    }

    //#endregion

    //#region Methods

    public async addAsync(model: AddComponentViewModel): Promise<Component> {
        const id = randomUUID();
        const component = new Component(id);
        component.kind = model.kind;
        component.name = model.name;
        component.description = model.description;
        component.availability = model.availability;
        component.createdTime = this._timeContext.getDate().getTime();

        const session = this._databaseContext.session();
        const szQuery = `MATCH (application:${CollectionLabels.application})-[:${EntityRelationships.hasVersion}]->(version:${CollectionLabels.applicationVersion})
                        WHERE application.id = '${model.applicationId}' and version.id = '${model.versionId}'
                        CREATE (version)-[:${EntityRelationships.hasComponent}]->(component:${CollectionLabels.component} $instance)
                        RETURN component`;

        const result = await session.run(szQuery, {
            instance: component
        });
        await session.close();

        const item = result.records[0].get('component').properties;
        return lodashMerge(new Component(id), item);
    }

    public async changeAvailabilityAsync(id: string, availability: ItemAvailabilities): Promise<void> {
        const session = this._databaseContext.session();
        await session.run(`MATCH (n: ${CollectionLabels.component})
        WHERE n.id = $componentId
        SET n.availability = $availability `, {
            componentId: id,
            availability: int(availability)
        });

        return void (0);
    }

    public async editAsync(model: EditComponentViewModel): Promise<Component> {
        const actualModel = lodashMerge(new EditComponentViewModel(model.id), model);
        const updatedParams = {};
        for (const key of Object.keys(actualModel)) {
            if (!actualModel[key]?.hasModified) {
                continue;
            }

            updatedParams[key] = actualModel[key].value;
        }

        const session = this._databaseContext.session();
        const updateResult = await session.run(
            `MATCH (n: ${CollectionLabels.component} {id: $componentId}) SET n += $params RETURN n{.*} as item`,
            {
                componentId: model.id,
                params: updatedParams
            });

        return lodashMerge(new Component(null), updateResult.records[0]?.get('item'));
    }

    public async getByApplicationIdAsync(applicationId: string, versionId: string): Promise<Component[]> {
        const session = this._databaseContext.session();

        const szQuery = `MATCH (application:${CollectionLabels.application})-[:${EntityRelationships.hasVersion}]->(version:${CollectionLabels.applicationVersion})-[:${EntityRelationships.hasComponent}]->(component:${CollectionLabels.component})
        WHERE application.id = '${applicationId}' AND version.id = '${versionId}' RETURN component;`;
        const queryResult = await session.run(szQuery);
        await session.close();

        const items = queryResult.records.map(x => lodashMerge(new Component(null), x.get('component').properties));
        return items;
    }

    public async getByIdAsync(id: string): Promise<Component> {
        const session = this._databaseContext.session();
        const command = `MATCH (component:${CollectionLabels.component}) WHERE component.id = '${id}' RETURN component`;
        const queryResult = await session.run(command);
        await session.close();
        const item = queryResult.records[0].get('component');
        return lodashMerge(new Component(null), item.properties);
    }

    public async deleteByIdAsync(id: string): Promise<void> {
        const session = this._databaseContext.session();
        const command = `MATCH (component:${CollectionLabels.component}) WHERE component.id = '${id}' DETACH DELETE component`;
        await session.run(command);
        await session.close();
    }
    //#endregion

}
