import {Method} from '../../models/method';
import {AddMethodViewModel} from "../../../../../view-models/methods/add-method.view-model";
import {ItemAvailabilities} from "../../../../../enumerations/item-availabilities";
import {EditMethodViewModel} from "../../../../../view-models/methods/edit-method.view-model";

export interface IMethodService {

    //#region Methods

    // Add applications asynchronously.
    addAsync(model: AddMethodViewModel): Promise<Method>;

    // Get applications by id asynchronously.
    getByIdAsync(id: string): Promise<Method>;

    // Delete an applications asynchronously.
    changeAvailabilityAsync(id: string, availability: ItemAvailabilities): Promise<void>;

    // Edit applications asynchronously.
    editAsync(model: EditMethodViewModel): Promise<Method>;

    // Search for applications asynchronously.
    getByComponentIdAsync(componentId: string): Promise<Method[]>;

    deleteAsync(id: string): Promise<void>;

    //#endregion

}
