import {ApplicationVersion} from '../../models/application-version';
import {
  AddApplicationVersionViewModel
} from "../../../../../view-models/applications/add-application-version.view-model";
import {
  EditApplicationVersionViewModel
} from "../../../../../view-models/applications/edit-application-version.view-model";

export interface IApplicationVersionService {

    //#region Methods

    getByApplicationIdAsync(applicationId: string): Promise<ApplicationVersion[]>;

    getByIdAsync(versionId: string): Promise<ApplicationVersion>;

    addAsync(model: AddApplicationVersionViewModel): Promise<ApplicationVersion>;

    editAsync(versionId: string, model: EditApplicationVersionViewModel): Promise<ApplicationVersion>;

    deleteByIdAsync(versionId: string): Promise<void>;

    //#endregion

}
