import {Event} from '../../models/event';
import {ItemAvailabilities} from "../../../../../enumerations/item-availabilities";
import {AddEventViewModel} from "../../../../../view-models/events/add-event.view-model";
import {EditEventViewModel} from "../../../../../view-models/events/edit-event.view-model";

export interface IEventService {

    //#region Methods

    // Add applications asynchronously.
    addAsync(model: AddEventViewModel): Promise<Event>;

    deleteByIdAsync(eventId: string): Promise<void>;

    // Get applications by id asynchronously.
    getByIdAsync(id: string): Promise<Event>;

    // Delete an applications asynchronously.
    changeAvailabilityAsync(id: string, availability: ItemAvailabilities): Promise<void>;

    // Edit applications asynchronously.
    editAsync(model: EditEventViewModel): Promise<Event>;

    // Search for applications asynchronously.
    getByComponentIdAsync(componentId: string): Promise<Event[]>;

    //#endregion

}
