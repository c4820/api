import {Application} from '../../models/application';
import {AddApplicationViewModel} from "../../../../../view-models/applications/add-application.view-model";
import {ItemAvailabilities} from "../../../../../enumerations/item-availabilities";
import {EditApplicationViewModel} from "../../../../../view-models/applications/edit-application.view-model";

export interface IApplicationService {

    //#region Methods

    // Add applications asynchronously.
    addAsync(model: AddApplicationViewModel): Promise<Application>;

    // Get applications by id asynchronously.
    getByIdAsync(id: string): Promise<Application>;

    // Delete an applications asynchronously.
    changeAvailabilityAsync(id: string, availability: ItemAvailabilities): Promise<void>;

    // Edit applications asynchronously.
    editAsync(id: string, model: EditApplicationViewModel): Promise<Application>;

    // Search for applications asynchronously.
    getAsync(): Promise<Application[]>;

    deleteByIdAsync(id: string): Promise<void>;

    //#endregion

}
