import {Component} from '../../models/component';
import {AddComponentViewModel} from "../../../../../view-models/components/add-component.view-model";
import {ItemAvailabilities} from "../../../../../enumerations/item-availabilities";
import {EditComponentViewModel} from "../../../../../view-models/components/edit-component.view-model";

export interface IComponentService {

    //#region Methods

    // Add applications asynchronously.
    addAsync(model: AddComponentViewModel): Promise<Component>;

    // Get by id asynchronously.
    getByIdAsync(id: string): Promise<Component>;

    // Delete component by id asynchronously.
    deleteByIdAsync(id: string): Promise<void>;

    // Delete an applications asynchronously.
    changeAvailabilityAsync(id: string, availability: ItemAvailabilities): Promise<void>;

    // Edit applications asynchronously.
    editAsync(model: EditComponentViewModel): Promise<Component>;

    // Search for applications asynchronously.
    getByApplicationIdAsync(applicationId: string, versionId: string): Promise<Component[]>;

    //#endregion

}
