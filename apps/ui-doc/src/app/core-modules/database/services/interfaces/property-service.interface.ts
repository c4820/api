import {Property} from '../../models/property';
import {AddPropertyViewModel} from "../../../../../view-models/properties/add-property.view-model";

export interface IPropertyService {

    //#region Methods

    // Add a property to a component asynchronously.
    addToComponentAsync(componentId: string, model: AddPropertyViewModel): Promise<Property>;

    // Add a property to an event asynchronously.
    addToEventAsync(eventId: string, model: AddPropertyViewModel): Promise<Property>;

    // Add a property to a method asynchronously.
    addToMethodAsync(methodId: string, model: AddPropertyViewModel): Promise<Property>;

    getByIdAsync(id: string): Promise<Property>;

    deleteByIdAsync(id: string): Promise<void>;

    // Get by component id asynchronously.
    getByComponentIdAsync(componentId: string): Promise<Property[]>;

    // Get by event id asynchronously.
    getByEventIdAsync(eventId: string): Promise<Property[]>;

    // Get by method id asynchronously.
    getByMethodIdAsync(methodId: string): Promise<Property[]>;

    //#endregion

}
