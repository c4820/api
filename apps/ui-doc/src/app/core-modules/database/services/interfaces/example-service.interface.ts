import {Example} from '../../models/example';
import {AddExampleViewModel} from "../../../../../view-models/examples/add-example.view-model";
import {EditExampleViewModel} from "../../../../../view-models/examples/edit-example.view-model";

export interface IExampleService {

    //#region Methods

    getByIdAsync(id: string): Promise<Example>;

    getByComponentIdAsync(componentId: string): Promise<Example[]>;

    addAsync(command: AddExampleViewModel): Promise<Example>;

    editAsync(command: EditExampleViewModel): Promise<Example>;

    deleteByIdAsync(id: string): Promise<void>;

    //#endregion

}
