import {ITimeContext} from './time-context.interface';
import {Injectable} from '@nestjs/common';

@Injectable()
export class TimeContext implements ITimeContext {

    //#region Methods

    public getDate(): Date {
        return new Date();
    }

    //#endregion
}
