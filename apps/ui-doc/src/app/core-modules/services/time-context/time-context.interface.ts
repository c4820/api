export interface ITimeContext {

    //#region Methods

    getDate(): Date;

    //#endregion
}
