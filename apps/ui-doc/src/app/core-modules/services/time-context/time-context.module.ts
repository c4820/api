import {Module} from '@nestjs/common';
import {TIME_CONTEXT} from './injectors';
import {TimeContext} from './time-context';

@Module({
    providers: [
        {
            provide: TIME_CONTEXT,
            useClass: TimeContext
        }
    ],
    exports: [
        TIME_CONTEXT
    ]
})
export class TimeContextModule {

}
