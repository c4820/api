import {IAuthenticationResult} from './interfaces/authentication-result.interface';

export interface IAuthenticationService {

    //#region Methods

    authenticateAsync(code: string, redirectUri: string): Promise<IAuthenticationResult>;

    validateIdTokenAsync(idToken: string): Promise<void>;

    //#endregion

}
