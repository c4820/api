import {Module} from '@nestjs/common';
import {HttpModule} from '@nestjs/axios';
import {ConfigService} from '@nestjs/config';
import {IAuthenticationOptions} from './interfaces/authentication-options.interface';
import {AUTHENTICATION_SERVICE, AUTHENTICATION_SETTINGS} from './constants/injectors';
import {AuthenticationService} from './authentication.service';
import {
  AuthenticationConfigurationKeys
} from "../../../../constants/configuration-keys/authentication-configuration-keys";

@Module({
    imports: [
        HttpModule
    ],
    providers: [
        {
            provide: AUTHENTICATION_SETTINGS,
            useFactory: (configService: ConfigService) => {
                const options: IAuthenticationOptions = {
                    baseUrl: configService.get(AuthenticationConfigurationKeys.baseUrl),
                    clientId: configService.get(AuthenticationConfigurationKeys.clientId),
                    clientSecret: configService.get(AuthenticationConfigurationKeys.clientSecret),
                    issuer: configService.get(AuthenticationConfigurationKeys.issuer),
                    audience: configService.get(AuthenticationConfigurationKeys.audience)
                };

                return options;
            },
            inject: [ConfigService]
        },
        {
            provide: AUTHENTICATION_SERVICE,
            useClass: AuthenticationService
        }
    ],
    exports: [
        AUTHENTICATION_SETTINGS,
        AUTHENTICATION_SERVICE
    ]
})
export class AuthenticationServiceModule {

}
