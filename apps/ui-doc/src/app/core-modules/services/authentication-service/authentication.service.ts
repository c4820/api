import {IAuthenticationService} from './authentication-service.interface';
import {Inject, Injectable} from '@nestjs/common';
import {IAuthenticationResult} from './interfaces/authentication-result.interface';
import {HttpService} from '@nestjs/axios';
import {AUTHENTICATION_SETTINGS} from './constants/injectors';
import {IAuthenticationOptions} from './interfaces/authentication-options.interface';
import {AxiosError} from 'axios';
import {firstValueFrom} from 'rxjs';

@Injectable()
export class AuthenticationService implements IAuthenticationService {

    //#region Constructor

    public constructor(
        @Inject(AUTHENTICATION_SETTINGS)
        protected readonly _options: IAuthenticationOptions,
        protected readonly _httpClient: HttpService) {
    }

    //#endregion

    //#region Methods

    public async authenticateAsync(code: string, redirectUri: string): Promise<IAuthenticationResult> {

        const fullUrl = `${this._options.baseUrl}/oauth/token`;
        const body: Record<string, string> = {};
        body['grant_type'] = 'authorization_code';
        body['code'] = code;
        body['client_id'] = this._options.clientId;
        body['client_secret'] = this._options.clientSecret;
        body['redirect_uri'] = redirectUri;

        try {
            const apiResult = await firstValueFrom(this._httpClient.post(fullUrl, body));
            if (apiResult.status != 200) {
                throw 'INVALID_AUTHENTICATION_RESULT';
            }

            const data = apiResult.data;
            return {
                accessToken: data['id_token'],
                refreshToken: data['refresh_token'],
                tokenType: data['token_type']
            };
        } catch (exception) {
            const actualException = exception as AxiosError<Record<string, string>>;
            throw 'INVALID_AUTHENTICATION_RESULT';
        }
    }

    public validateIdTokenAsync(idToken: string): Promise<void> {
        return Promise.resolve(undefined);
    }

    //#endregion

}
