import { SetMetadata } from '@nestjs/common';

export const ALLOW_ANONYMOUS = 'allow-anonymous';
export const LENIENT_AUTHENTICATION = 'lenient-authentication';

export const AllowAnonymous = () => SetMetadata(ALLOW_ANONYMOUS, true);
export const LenientAuthentication = () => SetMetadata(LENIENT_AUTHENTICATION, true);
