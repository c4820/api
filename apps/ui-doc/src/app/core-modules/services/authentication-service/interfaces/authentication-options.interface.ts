export interface IAuthenticationOptions {

    //#region Properties

    clientId: string;

    clientSecret: string;

    baseUrl: string;

    issuer: string;

    audience: string;

    //#endregion

}
