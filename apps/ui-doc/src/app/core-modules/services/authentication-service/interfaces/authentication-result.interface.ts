export interface IAuthenticationResult {

    //#region Properties

    accessToken: string;

    refreshToken: string;

    tokenType: string;

    //#endregion

}
