import {DynamicModule, Module} from '@nestjs/common';
import {VALIDATION_SERVICE} from './constants/injectors';
import {ValidationService} from './services/validation.service';

@Module({
    imports: [],
    providers: []
})
export class ValidatorModule {

    //#region Methods

    public static forRoot(): DynamicModule {
        return {
            global: true,
            module: ValidatorModule,
            providers: [
                {
                    provide: VALIDATION_SERVICE,
                    useClass: ValidationService
                }
            ],
            exports: [
                VALIDATION_SERVICE
            ]
        }
    }

    //#endregion

}
