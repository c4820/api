import {ValidationFailure} from '../models/validation-failure';
import {ModuleRef} from '@nestjs/core';

export type ValidationFn<T> = (injector: ModuleRef, instance: T) => null | ValidationFailure | Promise<ValidationFailure> | Promise<null>;
export type ConditionFn<T> = (injector: ModuleRef, instance: T) => boolean | Promise<boolean>;
