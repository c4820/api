import {Injectable} from '@nestjs/common';
import {ValidationResult} from '../models/validation-result';
import {ValidationFn} from '../constants/data-types';
import {ValidatorBuilder} from './validator-builder';
import {ValidationFailure} from '../models/validation-failure';
import {IValidationService} from '../interfaces/validation-service.interface';
import {ModuleRef} from '@nestjs/core';
import {ValidationException} from "../../../../models/exceptions/validation.exception";

@Injectable()
export class ValidationService implements IValidationService {

    //#region Properties

    private readonly _keyToValidators: { [key: string]: ValidationFn<any>[] } = {};

    //#endregion

    //#region Constructors

    public constructor(private moduleRef: ModuleRef) {
    }

    //#endregion

    //#region Methods

    public with<T>(validatorBuilder: ValidatorBuilder<T>): void {
        const key = validatorBuilder.key;
        if (!key || !key.length) {
            return;
        }

        let addedValidatorFns = this._keyToValidators[key];
        if (!addedValidatorFns) {
            addedValidatorFns = [];
        }

        const validators = validatorBuilder.validators;
        for (const validator of validators) {
            addedValidatorFns.push(validator);
        }

        this._keyToValidators[key] = validators;
    }

    // Do validation asynchronously.
    public async validateAsync<T>(key: string, instance: T, throwException: boolean): Promise<ValidationResult> {
        if (!key || !key.length || !this._keyToValidators[key]) {
            return Promise.resolve(new ValidationResult());
        }

        const validatorFns = this._keyToValidators[key];
        const validationFailures: ValidationFailure[] = [];

        for (const validatorFn of validatorFns) {
            const validationFailure = validatorFn(this.moduleRef, instance);
            let actualValidationFailure = null;

            if (validationFailure instanceof Promise) {
                actualValidationFailure = await validationFailure;
            } else {
                actualValidationFailure = validationFailure;
            }

            if (actualValidationFailure == null) {
                continue;
            }

            validationFailures.push(actualValidationFailure);
        }

        if (validationFailures.length < 1) {
            return null;
        }

        if (!throwException) {
            return Promise.resolve(new ValidationResult(validationFailures));
        }

        throw new ValidationException(validationFailures);
    }

    //#endregion
}
