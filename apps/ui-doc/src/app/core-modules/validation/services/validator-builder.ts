import {ConditionFn, ValidationFn} from '../constants/data-types';

export class ValidatorBuilder<T> {

    //#region Properties

    protected readonly _childFunctions: ValidationFn<T>[] = [];

    //#endregion

    //#region Constructor

    public constructor(public readonly key: string) {
    }

    //#endregion

    //#region Accessors

    public get validators(): ValidationFn<T>[] {
        return this._childFunctions;
    }

    //#endregion

    //#region Methods

    public with(validatorFn: ValidationFn<T>): ValidatorBuilder<T> {
        this._childFunctions.push(validatorFn);
        return this;
    }

    public when(conditionFn: ConditionFn<T>, validatorFn: ValidationFn<T>): ValidatorBuilder<T> {

        const actualValidationFn: ValidationFn<T> = async (injector, instance) => {
            const conditionResult = conditionFn(injector, instance);
            if (typeof conditionResult === 'boolean') {
                if (!conditionResult) {
                    return null;
                }

                return validatorFn(injector, instance);
            }

            await conditionResult;
            if (!conditionResult) {
                return null;
            }

            return validatorFn(injector, instance);
        }

        this._childFunctions.push(actualValidationFn);
        return this;
    }

    //#endregion

}
