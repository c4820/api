import {ValidatorBuilder} from '../services/validator-builder';
import {ValidationResult} from '../models/validation-result';

export interface IValidationService {

    //#region Methods

    with<T>(builder: ValidatorBuilder<T>): void;

    validateAsync<T>(key: string, instance: T, throwException?: boolean): Promise<ValidationResult>;

    //#endregion

}
