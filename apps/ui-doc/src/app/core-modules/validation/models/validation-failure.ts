export class ValidationFailure {

    //#region Properties

    public constructor(
        public readonly property: string,
        public readonly message: string,
        public readonly messageCode: string,
        public readonly additionalData?: any) {
    }

    //#endregion

}
