import {ValidationFailure} from './validation-failure';

export class ValidationResult {

    //#region Properties

    public readonly valid: boolean;

    //#endregion

    //#region Constructor

    public constructor(public readonly failures?: ValidationFailure[]) {
        this.valid = !failures || !failures.length;
    }

    //#endregion

}
