import {ExtractJwt, Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Inject, Injectable} from '@nestjs/common';
import {AUTHENTICATION_SETTINGS} from '../../services/authentication-service/constants/injectors';
import {IAuthenticationOptions} from '../../services/authentication-service/interfaces/authentication-options.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    //#region Constructor

    public constructor(@Inject(AUTHENTICATION_SETTINGS)
                       protected readonly _options: IAuthenticationOptions) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: _options.clientSecret,
            issuer: _options.issuer,
            audience: _options.audience
        });
    }

    //#endregion

    //#region Methods

    public async validate(payload: any) {
        return {userId: payload.sub, username: payload.nickname, name: payload.name, picture: payload.picture};
    }

    //#endregion
}
