import {ExecutionContext, Injectable} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {Reflector} from '@nestjs/core';
import {
    ALLOW_ANONYMOUS,
    LENIENT_AUTHENTICATION
} from '../../services/authentication-service/decorators/authentication-metadata.decorator';
import {firstValueFrom, Observable} from 'rxjs';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {

    //#region Constructor

    public constructor(private reflector: Reflector) {
        super();
    }

    //#endregion

    //#region Methods

    public async canActivate(context: ExecutionContext): Promise<boolean> {

        const allowAnonymous = this.reflector.getAllAndOverride<boolean>(ALLOW_ANONYMOUS, [
            context.getHandler(),
            context.getClass(),
        ]);

        if (allowAnonymous) {
            return true;
        }

        try {
            const ableToActive = super.canActivate(context);
            let actualResult = false;

            if (ableToActive instanceof Promise) {
                actualResult = await ableToActive;
            } else if (ableToActive instanceof Observable) {
                actualResult = await firstValueFrom(ableToActive);
            } else {
                actualResult = ableToActive;
            }

            return actualResult;
        } catch (exception) {

            // Whether authentication is lenient or not.
            const lenientAuthentication = this.reflector.getAllAndOverride<boolean>(LENIENT_AUTHENTICATION, [
                context.getHandler(),
                context.getClass(),
            ]);

            if (lenientAuthentication) {
                return true;
            }

            throw exception;
        }
    }

    //#endregion
}
