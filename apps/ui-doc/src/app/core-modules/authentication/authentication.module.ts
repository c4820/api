import {Module} from '@nestjs/common';
import {AuthenticationServiceModule} from '../services/authentication-service/authentication-service.module';
import {APP_GUARD} from '@nestjs/core';
import {JwtAuthGuard} from './guards/jwt-auth.guard';
import {JwtModule} from '@nestjs/jwt';
import {PassportModule} from '@nestjs/passport';
import {JwtStrategy} from './strategies/jwt.strategy';

@Module({
    imports: [
        PassportModule,
        AuthenticationServiceModule,
        JwtModule.register({}),
    ],
    providers: [
        {
            provide: APP_GUARD,
            useClass: JwtAuthGuard
        },
        JwtStrategy
    ]
})
export class AuthenticationModule {
}
