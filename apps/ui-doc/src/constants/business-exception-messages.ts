export class BusinessExceptionMessages {

    //#region Properties

    public static readonly componentNotFound = 'Component is not found';

    public static readonly eventNotFound = 'Event is not found';

    public static readonly methodNotFound = 'Method is not found';

    public static readonly versionNotFound = 'Version is not found';

    public static readonly applicationNotFound = 'Application is not found';

    //#endregion

}
