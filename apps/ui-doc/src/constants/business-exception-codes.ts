export class BusinessExceptionCodes {

    //#region Properties

    public static readonly componentNotFound = 'COMPONENT_NOT_FOUND';

    public static readonly eventNotFound = 'EVENT_NOT_FOUND';

    public static readonly methodNotFound = 'METHOD_NOT_FOUND';

    public static readonly versionNotFound = 'VERSION_NOT_FOUND';

    public static readonly applicationNotFound = 'APPLICATION_NOT_FOUND';

    //#endregion

}
