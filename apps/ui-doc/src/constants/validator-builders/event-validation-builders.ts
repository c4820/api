
import {BuiltInValidators} from './built-in-validators';
import {AddComponentViewModel} from '../../view-models/components/add-component.view-model';
import {AddEventViewModel} from '../../view-models/events/add-event.view-model';
import {EditEventViewModel} from '../../view-models/events/edit-event.view-model';
import {ValidatorBuilder} from "../../app/core-modules/validation/services/validator-builder";

export const addEvent = 'ADD_EVENT';
export const editEvent = 'EDIT_EVENT';

//#region Validation builder

export const addEventValidatorBuilder = new ValidatorBuilder<AddEventViewModel>(addEvent)
    .with((injector, instance) => {
        return BuiltInValidators.required<AddEventViewModel>('componentId', instance.componentId, `componentId is required`, 'COMPONENT_ID_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddEventViewModel>('name', instance.name, `name is required`, 'NAME_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddComponentViewModel>('description', instance.description, `description is required`, 'DESCRIPTION_REQUIRED');
    });

export const editEventValidatorBuilder = new ValidatorBuilder<EditEventViewModel>(editEvent)
    .when((injector, instance) => {
        return instance?.name?.hasModified === true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditEventViewModel>('name', instance.name.value, `name is required`, 'NAME_REQUIRED');
    });

//#endregion
