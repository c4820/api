
import {BuiltInValidators} from './built-in-validators';
import {AddMethodViewModel} from '../../view-models/methods/add-method.view-model';
import {EditMethodViewModel} from '../../view-models/methods/edit-method.view-model';
import {ValidatorBuilder} from "../../app/core-modules/validation/services/validator-builder";

export const addMethod = 'ADD_METHOD';
export const editMethod = 'EDIT_METHOD';

//#region Validation builder

export const addMethodValidatorBuilder = new ValidatorBuilder<AddMethodViewModel>(addMethod)
    .with((injector, instance) => {
        return BuiltInValidators.required<AddMethodViewModel>('name', instance.name, `name is required`, 'NAME_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddMethodViewModel>('valueType', instance.valueType, `valueType is required`, 'VALUE_TYPE_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddMethodViewModel>('description', instance.description, `description is required`, 'DESCRIPTION_REQUIRED');
    });

export const editMethodValidatorBuilder = new ValidatorBuilder<EditMethodViewModel>(editMethod)
    .when((injector, instance) => {
        return instance.name != null && instance.name?.hasModified == true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditMethodViewModel>('name', instance.name.value, `name is required`, 'NAME_REQUIRED');
    })
    .when((injector, instance) => {
        return instance.valueType != null && instance.valueType?.hasModified == true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditMethodViewModel>('valueType', instance.valueType.value, `valueType is required`, 'VALUE_TYPE_REQUIRED');
    })

//#endregion
