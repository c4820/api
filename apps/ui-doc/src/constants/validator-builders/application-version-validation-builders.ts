
import {AddApplicationVersionViewModel} from '../../view-models/applications/add-application-version.view-model';
import {BuiltInValidators} from './built-in-validators';
import {EditApplicationVersionViewModel} from '../../view-models/applications/edit-application-version.view-model';
import {ValidatorBuilder} from "../../app/core-modules/validation/services/validator-builder";

export const addApplicationVersion = 'ADD_APPLICATION_VERSION';
export const editApplicationVersion = 'EDIT_APPLICATION_VERSION';

//#region Validation builder

export const addApplicationVersionValidatorBuilder = new ValidatorBuilder<AddApplicationVersionViewModel>(addApplicationVersion)
    .with((injector, instance) => {
        return BuiltInValidators.required('applicationId', instance.applicationId, `applicationId is required`, 'APPLICATION_ID_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required('title', instance.applicationId, `title is required`, 'TITLE_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddApplicationVersionViewModel>('description', instance.description, `description is required`, 'DESCRIPTION_REQUIRED');
    });

export const editApplicationVersionValidatorBuilder = new ValidatorBuilder<EditApplicationVersionViewModel>(editApplicationVersion)
    .when((injector, instance) => {
        return instance.title != null && instance.title?.hasModified == true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditApplicationVersionViewModel>('title', instance.title.value, `title is required`, 'TITLE_IS_REQUIRED');
    })
    .when((injector, instance) => {
        return instance.description != null && instance.description?.hasModified == true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditApplicationVersionViewModel>('description', instance.title.value, `description is required`, 'DESCRIPTION_IS_REQUIRED');
    })

//#endregion
