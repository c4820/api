
import {BuiltInValidators} from './built-in-validators';
import {AddExampleViewModel} from '../../view-models/examples/add-example.view-model';
import {EditExampleViewModel} from '../../view-models/examples/edit-example.view-model';
import {ValidatorBuilder} from "../../app/core-modules/validation/services/validator-builder";

export const addExample = 'ADD_EXAMPLE';
export const editExample = 'EDIT_EXAMPLE';

//#region Validation builder

export const addExampleValidatorBuilder = new ValidatorBuilder<AddExampleViewModel>(addExample)
    .with((injector, instance) => {
        return BuiltInValidators.required<AddExampleViewModel>('name', instance.title, `title is required`, 'APPLICATION_ID_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddExampleViewModel>('description', instance.description, `description is required`, 'DESCRIPTION_REQUIRED');
    });

export const editExampleValidatorBuilder = new ValidatorBuilder<EditExampleViewModel>(editExample)
    .when((injector, instance) => {
        return instance.title != null && instance.title?.hasModified == true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditExampleViewModel>('name', instance.title.value, `title is required`, 'NAME_REQUIRED');
    })
    .when((injector, instance) => {
        return instance.description != null && instance.description?.hasModified == true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditExampleViewModel>('description', instance.description.value, `description is required`, 'DESCRIPTION_REQUIRED');
    })

//#endregion
