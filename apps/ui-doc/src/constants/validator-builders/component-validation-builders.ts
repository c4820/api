
import {BuiltInValidators} from './built-in-validators';
import {AddComponentViewModel} from '../../view-models/components/add-component.view-model';
import {EditComponentViewModel} from '../../view-models/components/edit-component.view-model';
import {ValidatorBuilder} from "../../app/core-modules/validation/services/validator-builder";

export const addComponent = 'ADD_COMPONENT';
export const editComponent = 'EDIT_COMPONENT';

//#region Validation builder

export const addComponentValidatorBuilder = new ValidatorBuilder<AddComponentViewModel>(addComponent)
    .with((injector, instance) => {
        return BuiltInValidators.required<AddComponentViewModel>('applicationId', instance.applicationId, `applicationId is required`, 'APPLICATION_ID_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddComponentViewModel>('versionId', instance.versionId, `versionId is required`, 'VERSION_ID_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddComponentViewModel>('name', instance.name, `name is required`, 'NAME_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddComponentViewModel>('kind', instance.kind, `kind is required`, 'KIND_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddComponentViewModel>('description', instance.description, `description is required`, 'DESCRIPTION_REQUIRED');
    });

export const editComponentValidatorBuilder = new ValidatorBuilder<EditComponentViewModel>(editComponent)
    .when((injector, instance) => {
        return instance.name != null && instance.name?.hasModified == true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditComponentViewModel>('name', instance.name.value, `name is required`, 'NAME_REQUIRED');
    })
    .when((injector, instance) => {
        return instance?.kind?.hasModified === true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditComponentViewModel>('kind', instance.kind.value, `kind is required`, 'KIND_REQUIRED');
    })
    .when((injector, instance) => {
        return instance?.description?.hasModified === true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditComponentViewModel>('description', instance.description.value, `description is required`, 'DESCRIPTION_REQUIRED');
    });

//#endregion
