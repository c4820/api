import {BuiltInValidators} from './built-in-validators';
import {AddPropertyViewModel} from '../../view-models/properties/add-property.view-model';
import {ValidatorBuilder} from "../../app/core-modules/validation/services/validator-builder";

export const addProperty = 'ADD_PROPERTY';

//#region Validation builder

export const addPropertyValidatorBuilder = new ValidatorBuilder<AddPropertyViewModel>(addProperty)
    .with((injector, instance) => {
        return BuiltInValidators.required<AddPropertyViewModel>('name', instance.name, `name is required`, 'NAME_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddPropertyViewModel>('description', instance.description, `description is required`, 'DESCRIPTION_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddPropertyViewModel>('valueType', instance.valueType, `valueType is required`, 'VALUE_TYPE_REQUIRED');
    });

//#endregion
