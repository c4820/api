import {ValidationFailure} from "../../app/core-modules/validation/models/validation-failure";


export class BuiltInValidators {

    //#region Methods

    public static required<T>(
        propertyName: string | keyof T,
        value: any,
        message: string,
        messageCode: string): ValidationFailure | null {

        if (typeof value === 'string') {
            if (value.length > 0) {
                return null;
            }

            return new ValidationFailure(<string> propertyName, message, messageCode);
        }

        if (value != null) {
            return null;
        }

        return new ValidationFailure(<string> propertyName, message, messageCode);
    }

    //#endregion

}
