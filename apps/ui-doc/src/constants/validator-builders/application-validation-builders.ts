
import {BuiltInValidators} from './built-in-validators';
import {AddApplicationViewModel} from '../../view-models/applications/add-application.view-model';
import {EditApplicationViewModel} from '../../view-models/applications/edit-application.view-model';
import {ValidatorBuilder} from "../../app/core-modules/validation/services/validator-builder";

export const addApplication = 'ADD_APPLICATION';
export const editApplication = 'EDIT_APPLICATION';

//#region Validation builder

export const addApplicationValidatorBuilder = new ValidatorBuilder<AddApplicationViewModel>(addApplication)
    .with((injector, instance) => {
        return BuiltInValidators.required<AddApplicationViewModel>('name', instance.name, `applicationId is required`, 'APPLICATION_ID_REQUIRED');
    })
    .with((injector, instance) => {
        return BuiltInValidators.required<AddApplicationViewModel>('description', instance.description, `description is required`, 'DESCRIPTION_REQUIRED');
    });

export const editApplicationValidatorBuilder = new ValidatorBuilder<EditApplicationViewModel>(editApplication)
    .when((injector, instance) => {
        return instance.name != null && instance.name?.hasModified == true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditApplicationViewModel>('name', instance.name.value, `name is required`, 'NAME_REQUIRED');
    })
    .when((injector, instance) => {
        return instance.description != null && instance.description?.hasModified == true;
    }, (injector, instance) => {
        return BuiltInValidators.required<EditApplicationViewModel>('description', instance.description.value, `description is required`, 'DESCRIPTION_REQUIRED');
    })

//#endregion
