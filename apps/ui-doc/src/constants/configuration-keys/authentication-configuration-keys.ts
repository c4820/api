export class AuthenticationConfigurationKeys {

    //#region Properties

    public static readonly clientId = 'AUTHENTICATION__CLIENT_ID';

    public static readonly clientSecret = 'AUTHENTICATION__CLIENT_SECRET';

    public static readonly baseUrl = 'AUTHENTICATION__BASE_URL';

    public static readonly issuer = 'AUTHENTICATION__ISSUER';

    public static readonly audience = 'AUTHENTICATION__AUDIENCE';

    public static readonly algorithms = 'AUTHENTICATION__ALGORITHMS';

    //#endregion

}
