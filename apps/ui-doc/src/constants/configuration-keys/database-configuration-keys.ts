export class DatabaseConfigurationKeys {

    //#region Properties

    public static readonly databaseUri = 'DATABASE_URI';

    public static readonly databaseUsername = 'DATABASE_USERNAME';

    public static readonly databasePassword = 'DATABASE_PASSWORD';

    //#endregion

}
