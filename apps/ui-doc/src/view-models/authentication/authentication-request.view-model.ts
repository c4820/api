import {ApiProperty} from '@nestjs/swagger';

export class AuthenticationRequestViewModel {

    //#region Properties

    @ApiProperty({
        description: 'Authorization code which returns by identity provider.'
    })
    public readonly code: string;

    @ApiProperty({
        description: 'Url for redirecting after authentication is done successfully.'
    })
    public readonly redirectUri: string;

    //#endregion

    //#region Constructor

    public constructor(code: string,
                       redirectUri: string) {
        this.code = code;
        this.redirectUri = redirectUri;
    }

    //#endregion

}
