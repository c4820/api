import {EditableField} from '../../models/editable-field';

export class EditMethodViewModel {

    //#region Properties

    public name: EditableField<string>;

    public valueType: EditableField<string>;

    public availability: EditableField<string>;

    //#endregion

    //#region Constructor

    public constructor(public readonly id: string) {
    }

    //#endregion

}
