import {ItemAvailabilities} from '../../enumerations/item-availabilities';

export class AddMethodViewModel {

    //#region Properties

    public name: string;

    public valueType: string;

    public description: string;

    public availability: ItemAvailabilities;

    //#endregion

    //#region Methods

    public constructor(public readonly componentId: string) {
    }

    //#endregion

}
