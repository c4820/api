import {ApiProperty} from '@nestjs/swagger';

export class AddExampleViewModel {

    //#region Properties

    @ApiProperty({
        description: 'Id of component to which example is attached to.'
    })
    public readonly componentId: string;

    @ApiProperty({
        required: true,
        description: 'Title in the example page.'
    })
    public readonly title: string;

    @ApiProperty({
        required: false,
        description: 'Description of the example'
    })
    public readonly description: string;

    //#endregion

    //#region Constructor

    public constructor(id: string) {
        this.componentId = id;
    }

    //#endregion

}
