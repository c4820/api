import {EditableField} from '../../models/editable-field';
import {ApiProperty} from '@nestjs/swagger';

export class EditExampleViewModel {

    //#region Properties

    @ApiProperty({
        required: true,
        description: 'Id of example which needs being edited.'
    })
    public readonly id: string;

    @ApiProperty({
        type: EditableField,
        description: 'Title of the example'
    })
    public readonly title: EditableField<string>;

    @ApiProperty({
        type: EditableField,
        description: 'Description of example'
    })
    public readonly description: EditableField<string>;

    //#endregion

    //#region Constructor

    public constructor(id: string) {
        this.id = id;
    }

    //#endregion

}
