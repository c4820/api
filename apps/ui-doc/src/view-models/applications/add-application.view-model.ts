import {ApiProperty} from '@nestjs/swagger';

export class AddApplicationViewModel {

    //#region Properties

    @ApiProperty({
        description: 'Name of application'
    })
    public readonly name: string;

    @ApiProperty({
        description: 'Description of application'
    })
    public readonly description: string;

    //#endregion

    //#region Constructor

    public constructor(name: string,
                       description: string) {
        this.name = name;
        this.description = description;
    }

    //#endregion

}
