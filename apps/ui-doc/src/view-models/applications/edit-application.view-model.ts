import {EditableField} from '../../models/editable-field';

export class EditApplicationViewModel {

    //#region Properties

    // Name of applications
    public name: EditableField<string>;

    public description: EditableField<string>;

    //#endregion

}
