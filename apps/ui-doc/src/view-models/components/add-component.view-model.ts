import {ItemAvailabilities} from '../../enumerations/item-availabilities';

export class AddComponentViewModel {

    //#region Properties

    public name: string;

    public kind: string;

    public description: string;

    public availability: ItemAvailabilities;

    //#endregion

    //#region Constructor

    public constructor(public readonly applicationId: string,
                       public readonly versionId: string) {
    }

    //#endregion

}
