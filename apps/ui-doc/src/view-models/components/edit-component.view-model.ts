import {EditableField} from '../../models/editable-field';
import {ItemAvailabilities} from '../../enumerations/item-availabilities';

export class EditComponentViewModel {

    //#region Properties

    public name: EditableField<string>;

    public kind: EditableField<string>;

    public description: EditableField<string>;

    public availability: EditableField<ItemAvailabilities>;

    //#endregion

    //#region Constructor

    public constructor(public readonly id: string) {
    }

    //#endregion

}
