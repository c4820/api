import {ItemAvailabilities} from '../../enumerations/item-availabilities';

export class AddEventViewModel {

    //#region Properties

    public name: string;

    public description: string;

    public availability: ItemAvailabilities;

    //#endregion

    //#region Constructor

    public constructor(public readonly componentId: string) {
    }

    //#endregion

}
