import {ItemAvailabilities} from '../../enumerations/item-availabilities';

export class AddPropertyViewModel {

    //#region Properties

    public name: string;

    public description: string;

    public valueType: string;

    public defaultValue: string;

    public optional: boolean;

    public availability: ItemAvailabilities;

    //#endregion

}
