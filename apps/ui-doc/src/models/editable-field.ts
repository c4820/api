export class EditableField<T> {

    //#region Properties

    public constructor(public readonly value: T, public readonly hasModified: boolean) {

    }

    //#endregion

}
