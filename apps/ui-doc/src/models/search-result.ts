export class SearchResult<T> {

    //#region Constructor

    public constructor(public readonly items: T[],
                       public readonly totalRecords: number) {
    }

    //#endregion

}
