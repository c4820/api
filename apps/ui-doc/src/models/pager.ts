export class Pager {

    //#region Constructor

    public constructor(public readonly skip: number,
                       public readonly limit: number) {
    }

    //#endregion

}
