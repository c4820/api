import {BusinessException} from './business.exception';
import {HttpStatus} from '@nestjs/common';

export class ItemNotFoundException extends BusinessException {

    //#region Constructor

    public constructor(code: string, message: string) {
        super(code, message, HttpStatus.NOT_FOUND);
    }

    //#endregion

}
