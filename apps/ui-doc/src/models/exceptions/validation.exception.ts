import {ValidationFailure} from "../../app/core-modules/validation/models/validation-failure";


export class ValidationException {

    //#region Constructor

    public constructor(public readonly validationFailures: ValidationFailure[]) {
    }

    //#endregion
}
