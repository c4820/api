import {HttpStatus} from '@nestjs/common';

export class BusinessException {

    //#region Constructor

    public constructor(
        public readonly code: string,
        public readonly message: string = '',
        public readonly statusCode: HttpStatus | number = HttpStatus.INTERNAL_SERVER_ERROR) {
    }

    //#endregion

}
