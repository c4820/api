export class DatabaseConfiguration {

    //#region Properties

    public uri: string;

    public username: string;

    public password: string;

    //#endregion

}
