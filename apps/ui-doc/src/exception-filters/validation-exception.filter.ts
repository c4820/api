import {ArgumentsHost, Catch, ExceptionFilter, HttpStatus} from '@nestjs/common';
import {ValidationException} from '../models/exceptions/validation.exception';
import {Response} from 'express';

@Catch(ValidationException)
export class ValidationExceptionFilter implements ExceptionFilter {

    //#region Methods

    public catch(exception: ValidationException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        const validationFailures = exception.validationFailures!;
        const propertyNameToValidationFailures: Record<string, any> = {};

        for (const validationFailure of validationFailures) {

            let actualFailures = propertyNameToValidationFailures[validationFailure.property];
            if (!actualFailures) {
                actualFailures = [];
                propertyNameToValidationFailures[validationFailure.property] = actualFailures;
            }

            actualFailures.push({
                code: validationFailure.messageCode,
                message: validationFailure.message,
                additionalData: validationFailure.additionalData
            });
        }

        response
            .status(HttpStatus.BAD_REQUEST)
            .json(propertyNameToValidationFailures);
    }

    //#endregion

}
