import {ArgumentsHost, Catch, ExceptionFilter} from '@nestjs/common';
import {Response} from 'express';
import {BusinessException} from '../models/exceptions/business.exception';

@Catch(BusinessException)
export class BusinessExceptionFilter implements ExceptionFilter {

    //#region Methods

    public catch(exception: BusinessException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const status = exception.statusCode;

        response
            .status(status)
            .json({
                code: exception.code,
                message: exception.message
            });
    }

    //#endregion

}
