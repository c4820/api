# Library API

Provide a small api system that serves a website which is about library apis & demos.

## 1. Conventions & rules

This project requires a strict coding convention & coding structure.
Developers must follow those instructions to keep code structure clearer.

### 1.1. Coding structure
- TBD

### 1.2. Coding convention
- TBD

## 2. Development

### 2.1. Consistent build
Normally, Angular project uses NodeJS to:
- Build the source code.
- Host a http server for the built source code.

NodeJS can be different between developers' machine & sometimes, the latest version of NodeJS causes issues while building the application (due to some functions are not supported anymore).

At the time this project was done, the version **v17.2.0** of NodeJS was used.

To keep builds consistent between developers, the below command must be used:

```
docker run -it --rm -v ${PWD}/:/app node:17.2-alpine3.14 /bin/sh -c 'cd /app; npm install --legacy-peer-deps'
```

### 2.2. Development environment
This project contains a common [docker-compose.yml](docker-compose.yml) file to help developers setup services that is necessary to run the system.

In addition, [docker-compose.development.yml](docker-compose.development.yml) file is used during development stage.

- To tear down started containers, the follow command should be used:
```
docker-compose --project-name ui-tool-api down
```

- To start the containers, the following command should be used:
```
docker-compose --project-name ui-tool -f docker-compose.yml up --detach
```

### 2.3. Build
```
docker build -t ui-tool-api:1.0.0 -t ui-tool-api:latest .
```

## 3. Deployment

### Windows | Linux | MacOS:

- After finishing developing the application. The following command can be used for building the application;
```
npm run build
```

- Build process will produce these following files:
```
- main.js
- tsconfig.build.tsbuildinfo
```

- Following [this tutorial](https://blog.cloudboost.io/nodejs-pm2-startup-on-windows-db0906328d75) to install & deploy application using [PM2 - Process manager 2](https://pm2.keymetrics.io/docs/usage/quick-start/).

### Hosting heroku:
- Follow [this tutorial on medium](https://medium.com/weekly-webtips/deploying-a-nestjs-app-with-heroku-5fa84cb5b6c6) to deploy node application onto heroku hosting.


## Environment variables
- Instead of hard coding dynamic values, this project uses these below environment variables to give users more control in the settings:

### Database

---
`DATABASE_URI`: Connection string to [neo4j database](https://neo4j.com/).

`DATABASE_USERNAME`: Username to access to the above neo4j database.

`DATABASE_PASSWORD`: Password to sign in the above neo4j database.


### Auth0
This project uses [auth0](https://auth0.com/) as an identity provider.
The below variables are for exchaning `authorization code` with `access token` as mentioned in [this tutorial](https://auth0.com/docs/get-started/authentication-and-authorization-flow/add-login-auth-code-flow#post-to-token-url-example).

---

`AUTHENTICATION__BASE_URL`: _https://YOUR_DOMAIN_ parameter which mentioned in [this section](https://auth0.com/docs/get-started/authentication-and-authorization-flow/add-login-auth-code-flow#authorization-url-example).

`AUTHENTICATION__CLIENT_ID`: Client id which is provided by auth0.

`AUTHENTICATION__CLIENT_SECRET`: Client secret which is provided by auth0.

`AUTHENTICATION__ISSUER`: _Same as the_ **AUTHENTICATION__BASE_URL**

`AUTHENTICATION__AUDIENCE`: Audience which is provided by auth0.

**NOTE**: This project credential validation only supports HS256 JWT, therefore, please change the **signing algorithm** to **HS256**. [This tutorial](https://community.auth0.com/t/getting-rs256-token-instead-of-hs256/84827) can help.

